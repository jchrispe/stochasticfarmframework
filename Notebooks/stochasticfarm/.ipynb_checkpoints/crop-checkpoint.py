from math import *
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import GPy
from IPython.display import display
from scipy.stats import gamma, gaussian_kde

class crop:
    def __init__(self,name,area,value,Ky,Ym,Kc,p,Zr,harvestDate,plantDate,simYears):
        self.name  = name                # Crop Name
        self.area  = area                # Acres of total farm area with this crop on it.
        self.value = value               # $/metric ton
        self.Kc = Kc                     # Crop coefficient - dimensionless
        self.Ky = Ky                     # Crop water production response coefficient 
        self.p  = p                      # p is root zone depleation coefficient
        self.Zr = Zr                     # Max rooting depth (m)
        self.Ym = Ym                     # Max Yield Metric Ton per Acre
        self.harvestDate = harvestDate   # Date when crop is harvested (day of the year)
        self.plantDate = plantDate       # Date when crop is planted (in date, out date)
        
        self.simYears = simYears
        self.TAW      = None             # Total available water to be updated later by farm class.
        self.MaxIPerDay = 1e99           # Maximum Amound of irrigation allowed per acre for this crop. 
        
        #### (Set based on the growing dates given above)
        self.yearlyGrowing = np.zeros(365)  # A Boolean array that denotes the growing season of this crop.
        self.yearlyHarvest = np.zeros(365)  # A Boolean array that denotes the havest dates (yearly)
        
        for i in range(len(harvestDate)):
            self.yearlyHarvest[harvestDate[i]] = 1.0
        for i in range(0,len(plantDate),2):
            self.yearlyGrowing[plantDate[i]:plantDate[i+1]] = 1.0
            
        # Repeat the growing season over forcasted years. 
        self.yearlyHarvest = np.tile(self.yearlyHarvest,self.simYears)
        self.yearlyGrowing = np.tile(self.yearlyGrowing,self.simYears)
        
        # Set the Kc values as a vector based on the yearly growing plant Harvest.
        # The values should vary across the growing season. 
        self.YearlyKc = self.SetYearlyKcValues()
        self.YearlyKc = np.tile(self.YearlyKc,self.simYears)
        
    def setMaxIrrigationPerDay(self,MaxIPerDay):
        # Here max irrigation per day is coming in using units of [mm/day]
        self.MaxIPerDay = MaxIPerDay
      
    def PlotQuantity(self,vector,name, ax=None):
        if(ax is None):
            fig, ax = plt.subplots()
        xpts = np.linspace(0,len(vector),len(vector))
        ax.plot(xpts, vector,label=name)
        ax.set_xlabel('Time [days]')
        ax.set_ylabel(name)
        
    def SetYearlyKcValues(self):
        # Linear function used to find value between points.
        def line(xs,xe,ys,ye,t):
            m = ((ys-ye)/(xs-xe))
            return m*(t-xs) + ys

        yearlyKc    = np.zeros(365)
        nsubseasons = int(len(self.plantDate)/2)
        harvests    = int(len(self.harvestDate))
        print("Crop ", self.name, " :", nsubseasons, " growing periods per year.")
        print("     and ", harvests, "harvests per year season.")
        
        if (nsubseasons == harvests):
            for i in range(nsubseasons):
                dist = self.plantDate[2*i+1] - self.plantDate[2*i]
                s0 = self.plantDate[2*i]
                s1 = s0 + int(0.25*dist)
                s2 = s0 + int(0.5*dist)
                s3 = s0 + int(0.75*dist)
                s4 = self.plantDate[2*i+1]
                
                for j in range(s0,s1):
                    yearlyKc[j] = self.Kc[0]
                for j in range(s1,s2):
                    yearlyKc[j] = line(s1,s2,self.Kc[0],self.Kc[1],j)
                for j in range(s2,s3):
                    yearlyKc[j] = self.Kc[1]
                for j in range(s3,s4):
                    yearlyKc[j] = line(s3,s4,self.Kc[1],self.Kc[2],j)
                    
        if (nsubseasons != harvests):
            for i in range(harvests):
                if (i == 0):
                    dist = self.harvestDate[i] - self.plantDate[i]
                    s0 = self.plantDate[i]
                    s1 = s0 + int(0.25*dist)
                    s2 = s0 + int(0.5*dist)
                    s3 = s0 + int(0.75*dist)
                    s4 = self.harvestDate[i]
                if (i > 0):
                    dist = self.harvestDate[i] - self.harvestDate[i-1]
                    s0 = self.harvestDate[i-1]
                    s1 = s0 + int(0.25*dist)
                    s2 = s0 + int(0.5*dist)
                    s3 = s0 + int(0.75*dist)
                    s4 = self.harvestDate[i]
                    
                for j in range(s0,s1):
                    yearlyKc[j] = self.Kc[0]
                for j in range(s1,s2):
                    yearlyKc[j] = line(s1,s2,self.Kc[0],self.Kc[1],j)
                for j in range(s2,s3):
                    yearlyKc[j] = self.Kc[1]
                for j in range(s3,s4):
                    yearlyKc[j] = line(s3,s4,self.Kc[1],self.Kc[2],j)
                    
            if (self.harvestDate[-1] < self.plantDate[-1]):
                for j in range(self.harvestDate[-1],self.plantDate[-1]):
                    yearlyKc[j] = self.Kc[0]
            
        return yearlyKc

    def Yield(self,ET0,Rain,ThetaFC,ThetaWP,WaterCost, randThetaUni=None):

        # ET0 coming in is assumed to be in mm/day
        # Rain coming in is assumed to be in ft/day. Note 1 ft = 304.8 mm 
        ft2mm = 304.8      # mm/ft
        mm2ft = 0.00328084 # ft/mm
        m2mm  = 1000       # mm/m

        # want ETa = Ks*Kc*ET0
        # To get Ks we need: 
        # Ks = (TAW - Dr)/((1-p)TAW) when Dr > (p*TAW)   --> ET_a = Kc*Ks*ET0
        # Ks = 1.0                   when Dr <= (p*TAW)  --> ET_a = Kc*ET0
        # Here TAW is the total available water a fixed quantity based on the soil and crop grown.
        # So the unknown time dependent quantity is Dr (the root zone deRpletion [mm])
        # Dr_i = Dr_{i-1} - (P - RO)_i - I_i - CR_i + ET_a,i + DP_i    for Dr in [0, TAW]
        # with: 
        # 
        # P    - percipitation.
        # RO   - runoff on soil surface
        # I    - net irrigation
        # CR   - capillary rise from groundwater table (Assumed zero when water table is a meter below root zone).
        # ETa  - crop evapotranspirtion 
        # DP   - water loss out the root zone by deep percolation (Assumed zero for now in model).
        # 
        # The initial value of Dr_{i-1} is given by:
        # Dr_{i-1}= 1000(Theta_FC - Theta_{i-1})Zr
        # Here we take a value for Theta_{i-1} to be randomly between Theta_FC and Theta_WP   
        
        #=========================================================================
        # THREE VERSIONS OF CROP GROWTH: 
        #    1) _noi : No irrigation allowed
        #    2) _lim : Irrigation limited by water restriction.
        #    3) _ful : Irrigation allowed to ensure non-stressed crop growth. 
        #=========================================================================        
        
        self.Dr_lim       = np.zeros(365*self.simYears) # Root zone depleation with limited irrigation.
        self.Dr_noi       = np.zeros(365*self.simYears) # Root zone depleation with no irrigation. 
        self.Dr_ful       = np.zeros(365*self.simYears) # Root zone depleation with full irrigation.
        
        self.Ks_lim       = np.zeros(365*self.simYears) # Stressed crop growth factor with limited irrigation. 
        self.Ks_noi       = np.zeros(365*self.simYears) # Stressed crop growth factor with no irrigation.
        self.Ks_ful       = np.zeros(365*self.simYears) # Stressed crop growth factor with full irrigation. 
        
        self.ETa_lim      = np.zeros(365*self.simYears) # Actual crop evapotransporation with limited irrigation. 
        self.ETa_noi      = np.zeros(365*self.simYears) # Actual crop evapotransporation with no irrigation.
        self.ETa_ful      = np.zeros(365*self.simYears) # Actual crop evapotransporation with full irrigation. 
        
        self.I_lim        = np.zeros(365*self.simYears) # Irrigation allowed to infiltrate soil on ith day in mm limited by max value.
        self.I_noi        = np.zeros(365*self.simYears) # Academically at zero for no irrigation. 
        self.I_ful        = np.zeros(365*self.simYears) # Irrigation allowed to infiltrate soil on ith day when unlimited limits allowed.

        self.ICost_lim    = np.zeros(365*self.simYears) # Cost of irrigation on day i in simulation limited daily value assumption. 
        self.ICost_noi    = np.zeros(365*self.simYears) # Cost of irrigation on day i in simulation. No irrigation (Should be zero). 
        self.ICost_ful    = np.zeros(365*self.simYears) # Cost of irrigation on day i in simulation. Unlimited water use cost.  
        
        self.IDeficit_lim = np.zeros(365*self.simYears) # Deficit water needed for non stressed growth (limited irrigation), partial sanity check. 
        self.IDeficit_noi = np.zeros(365*self.simYears) # Deficit water needed for non stressed growth (no irrigaiton), partial sanity check. 
        self.IDeficit_ful = np.zeros(365*self.simYears) # Deficit water needed for non stressed growth (full irrigaion), sanity check. 
        
        RAW    = self.p*self.TAW # The readily avalilable soil water space in the rootzone.
        RunOff = 0.0             # 0 --> Considered as part of the stochastic nature of other quantities in current model. 
        
        # Set an initial rootzone depletion value (Start of the first day all formats the same!)
        if(randThetaUni is None):
            randThetaUni = np.random.rand()  
            
        ThetaIMO        = ThetaWP + randThetaUni*(ThetaFC-ThetaWP)       
        self.Dr_lim[0]  = m2mm*(ThetaFC - ThetaIMO)*self.Zr
        self.Dr_noi[0]  = m2mm*(ThetaFC - ThetaIMO)*self.Zr
        self.Dr_ful[0]  = m2mm*(ThetaFC - ThetaIMO)*self.Zr
        
        # Set the initial value of the irrigation. 
        self.I_lim[0]   = min(self.MaxIPerDay,max(self.Dr_lim[0] - RAW,0)*self.yearlyGrowing[0]) 
        #self.I_noi[0]  is initially set to zero.  
        self.I_ful[0]   = max(self.Dr_ful[0] - RAW,0)*self.yearlyGrowing[0] 
        
        # Set an irrigation deficit. 
        self.IDeficit_lim[0] = max(max(self.Dr_lim[0] - RAW,0)*self.yearlyGrowing[0]-self.I_lim[0],0)
        self.IDeficit_noi[0] = max(max(self.Dr_noi[0] - RAW,0)*self.yearlyGrowing[0]-self.I_noi[0],0)
        self.IDeficit_ful[0] = max(max(self.Dr_lim[0] - RAW,0)*self.yearlyGrowing[0]-self.I_ful[0],0)
        
        for i in range (len(Rain)): 
            # Each Day
            # (Step 1): Set the Ks value.
            self.Ks_lim[i] = min(1.0,(self.TAW - self.Dr_lim[i])/((1.0-self.p)*self.TAW))
            self.Ks_noi[i] = min(1.0,(self.TAW - self.Dr_noi[i])/((1.0-self.p)*self.TAW))
            self.Ks_ful[i] = min(1.0,(self.TAW - self.Dr_ful[i])/((1.0-self.p)*self.TAW))
                    
            # (Step 2): Set the ETa for the crop based on a the Ks found using Lagged Dr value.
            self.ETa_lim[i] = self.Ks_lim[i]*self.YearlyKc[i]*ET0[i]    
            self.ETa_noi[i] = self.Ks_noi[i]*self.YearlyKc[i]*ET0[i]    
            self.ETa_ful[i] = self.Ks_ful[i]*self.YearlyKc[i]*ET0[i]    
            
            # (Step 3): Set Root zone depleation and irrigation values based on ETa, and Rain.
            if(i!=len(Rain)-1):                
                # Other stuff happening here for runoff and irrigation. (Max Irrigation Per day is in [mm])
                Dr_lim = max(0.0,self.Dr_lim[i] - (Rain[i]*ft2mm - RunOff) + self.ETa_lim[i])  # Never negative
                Dr_noi = max(0.0,self.Dr_noi[i] - (Rain[i]*ft2mm - RunOff) + self.ETa_noi[i])  # Never negative
                Dr_ful = max(0.0,self.Dr_ful[i] - (Rain[i]*ft2mm - RunOff) + self.ETa_ful[i])  # Never negative
                
                self.I_lim[i+1] = min(self.MaxIPerDay,max(Dr_lim - RAW,0)*self.yearlyGrowing[i]) # Zero if not growing. 
                #self.I_noi[i+1] Is initially set to zero.
                self.I_ful[i+1] = max(Dr_ful - RAW,0)*self.yearlyGrowing[i] # Zero if not growing.
                
                self.Dr_lim[i+1] = min(Dr_lim - self.I_lim[i+1],self.TAW)   # Never bigger than total available water
                self.Dr_noi[i+1] = min(Dr_noi,self.TAW)                     # Never bigger than total available water
                self.Dr_ful[i+1] = min(Dr_ful - self.I_ful[i+1],self.TAW)   # Never bigger than total available water
                
                # Have - Want is negative --> deficit. This is done somewhat as a sanity check in the code.  
                if((self.I_lim[i+1] - max(self.Dr_lim[i+1] - RAW,0)*self.yearlyGrowing[i]) < 0.0):
                    self.IDeficit_lim[i+1] = max(max(self.Dr_lim[i+1] - RAW,0)*self.yearlyGrowing[i+1]-self.I_lim[i+1],0)
                if((self.I_noi[i+1] - max(self.Dr_noi[i+1] - RAW,0)*self.yearlyGrowing[i]) < 0.0):
                    self.IDeficit_noi[i+1] = max(max(self.Dr_noi[i+1] - RAW,0)*self.yearlyGrowing[i+1]-self.I_noi[i+1],0)
                if((self.I_ful[i+1] - max(self.Dr_ful[i+1] - RAW,0)*self.yearlyGrowing[i]) < 0.0):
                    self.IDeficit_ful[i+1] = max(max(self.Dr_ful[i+1] - RAW,0)*self.yearlyGrowing[i+1]-self.I_ful[i+1],0)
                
        ETm = self.YearlyKc*ET0*self.yearlyGrowing  
        
        # ========================================================================================
        # Notes on daily Yield Computation:
        # ========================================================================================
        # 1. As ETm could be the same as ETa, we replace 0.0/0.0 --> NaN case is replced with 1.0.
        # 2. We only grow when crops are planted. Multiply by yearlyGrowing (0=out, 1=in) ground.
        # 3. Looking for a daily yield. Scale by fraction the Ym (yield max) to daily growth.
        # 4. The daily yield should never be negative. 
        
        fracOfYear  = np.count_nonzero(self.yearlyGrowing)/len(self.yearlyGrowing)
        dailyYmFrac = (1.0/((fracOfYear)*365.0))
        
        self.dailyYieldVec_lim = dailyYmFrac*self.Ym*(1.0 - self.Ky*(1.0 - ((self.ETa_lim/ETm).replace({np.NaN : 1.0}))))*self.yearlyGrowing
        self.dailyYieldVec_noi = dailyYmFrac*self.Ym*(1.0 - self.Ky*(1.0 - ((self.ETa_noi/ETm).replace({np.NaN : 1.0}))))*self.yearlyGrowing
        self.dailyYieldVec_ful = dailyYmFrac*self.Ym*(1.0 - self.Ky*(1.0 - ((self.ETa_ful/ETm).replace({np.NaN : 1.0}))))*self.yearlyGrowing
        
        self.dailyYieldVec_lim[self.dailyYieldVec_lim < 0] = 0.0
        self.dailyYieldVec_noi[self.dailyYieldVec_noi < 0] = 0.0
        self.dailyYieldVec_ful[self.dailyYieldVec_ful < 0] = 0.0
        
        #self.PlotQuantity(self.dailyYieldVec,"daily yield "+self.name)
        
        # Do Irrigation cost and deficit cost. 
        self.ICost_lim = self.I_lim*mm2ft*self.area*WaterCost   # Cost of irrigation (acre feet)
        self.ICost_noi = self.I_noi*mm2ft*self.area*WaterCost   # Cost of irrigation (acre feet)
        self.ICost_ful = self.I_ful*mm2ft*self.area*WaterCost   # Cost of irrigation (acre feet)
        
        self.IDeficitCost_lim = self.IDeficit_lim*mm2ft*self.area*WaterCost # Deficit cost of irrigation in (acre feet)
        self.IDeficitCost_noi = self.IDeficit_noi*mm2ft*self.area*WaterCost # Deficit cost of irrigation in (acre feet) Sanity check!
        self.IDeficitCost_ful = self.IDeficit_ful*mm2ft*self.area*WaterCost # Deficit cost of irrigation in (acre feet)        
        
        return self.dailyYieldVec_lim, self.dailyYieldVec_noi, self.dailyYieldVec_ful

    def CropValue(self,ET0,Rain,ThetaFC,ThetaWP,WaterCost):
        # This is a daily crop value, as cropYield is a daily fraction of an annual value. 
        cropYield_lim, cropYield_noi, cropYield_ful = self.Yield(ET0,Rain,ThetaFC,ThetaWP,WaterCost)
        self.CropValue_lim = self.area*self.value*cropYield_lim 
        self.CropValue_noi = self.area*self.value*cropYield_noi
        self.CropValue_ful = self.area*self.value*cropYield_ful

    def displayCrop(self):
        print("   Crop {0}".format(self.name))
        print("        {0} acres".format(self.area))
        print("        {0} value ($/metric ton)".format(self.value))
        print("        {0} Ky".format(self.Ky))
        print("        {0} Ym (metric tons/acre)".format(self.Ym))
        print("        {0} Kc".format(self.Kc))
        print("        {0} p".format(self.p))
        print("        {0} Zr (m)".format(self.Zr))
        print("        {0} Harvest Date".format(self.harvestDate))
        print("        {0} Plant Date".format(self.plantDate))
        print("        {0} Farm Specific TAW root zone (mm)".format(self.TAW))
        print("   ----------------------------------------------------------------  ")
        
    def showSeasonDates(self):
        plt.figure()
        plt.plot(self.yearlyGrowing,'.',label='Growing Season')
        plt.plot(self.yearlyHarvest,'*',label='Harvest Date')
        plt.plot(self.YearlyKc,'-',label='Seasonal Kc')
        plt.legend(loc=1)
        plt.ylim((0, 1.3))
        plt.xlabel('Day of Year')
        plt.ylabel(self.name +' In Ground')
        
        SaveName = "GrowingSeason" + self.name + ".pdf"
        plt.savefig(SaveName)
     


