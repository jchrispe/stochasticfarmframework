#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

from math import *
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import GPy
from IPython.display import display
from scipy.stats import gamma, gaussian_kde

class environment:
    def __init__(self,futureYears):
        self.futureData  = None         # Holds a random realization of future environmental data. 
        self.histData    = None         # Holds historical data (e.g., observations from CIMIS)
        self.futureET0   = None         # Holds ET0 computation based on future environmental data. 
        self.histET0     = None         # Holds ET0 computed based on the historical data used in model training. 
        self.futureYears = futureYears  # Number of years to look into the future. 
        
    def train(self, histData):
        """ Compute statistical descriptions of historical data that enables us to draw
            random samples of future environmental variables.
        """
        self.histData = histData
        self.histData['Fractional Year'] = np.arange(0,self.histData.shape[0],1)/365.0
        
        self.trainRadiation()
        self.trainTemperature()
        self.trainWind()
        self.trainPressure()  
        self.trainPrecip()
        self.histET0 = self.computeET0(self.histData)
        
        
    def sample(self):
        """ Draw a random sample of future environmental parameters.
        """

        self.sampleRadiation()
        self.sampleTemperature()
        self.sampleWind()
        self.samplePressure()
        self.samplePrecip()
        self.futureET0 = self.computeET0(self.futureData)
        
        
    def displayData(self,SaveName=None):
        print("Environment Data:")
        self.plotTrainedData(SaveName)
        #self.displayET0()
        
    def computeET0(self,df):
        """ This function follows the FA056 paper referenced above. 
            Pull the needed components out of the models environment. 
        """

        Rn       = df['Net Radiation [MJ/m^2/day]']
        T        = df['Air Temperature [C]']
        u2       = df['Wind Speed [m/s]']
        ea       = df['Vapor Pressure [kPa]']
        atmPress = 30*3.38639*np.ones(df.shape[0]) 
        G        = 0.0 
        
        # Ensure these are in the proper units.     
        #conv      = 0.000136701662 #mm/day to feet/hour
        conv      = 1.0 # Keep ET0 in mm/day
        es        = 0.6108 * np.exp(17.27 * T / (T+237.3))
        es[es<ea] = ea
        delta     = 4098 * es / (T+237.3)**2
        Gamma     = 6.65e-4 * atmPress
    
        ET0 = conv*(0.408 * delta * (Rn-G) + Gamma *(900.0/(T+273.0)) * u2 * (es-ea))/(delta + Gamma*(1.0 + 0.34*u2))
        
        # Ensure that ET0 computation is never negative. 
        ET0 = ET0.clip(0.0,None)
        
        return ET0 
        
    def displayET0(self):
        print("   Info about ET0: ")
        print("       Training Data ET0:")
        print(self.histET0)
        print("       Future Predicted ET0:")
        print(self.futureET0)
            
    #============================================
    # TRAIN ENVIRONMENT BASED ON HISTORIC DATA
    #============================================    

    def trainRadiation(self):
        thinBy = 4
        
        obs = self.histData['Net Radiation [MJ/m^2/day]'].iloc[0::thinBy].values
        time = self.histData['Fractional Year'].iloc[0::thinBy] #np.arange(0,obs.shape[0]*thinBy,thinBy)/365.0 # time, in years

        # Remove NaN's from the data because that will cause GPy to act up
        time = time[~np.isnan(obs)][:,None]
        obs = obs[~np.isnan(obs)][:,None]

        # Define each part of the covariance kernels
        periodKernel = ( GPy.kern.PeriodicMatern32(input_dim=1, variance=1.0, lengthscale=0.1, period=1)
                              * GPy.kern.RBF(input_dim=1, variance=10., lengthscale=5.0)  )
        longTrendKernel = GPy.kern.RBF(input_dim=1, variance=1., lengthscale=7.0)
        shortTrendKernel = GPy.kern.RBF(input_dim=1, variance=0.2, lengthscale=0.1)

        # Combine the kernels
        kernel = periodKernel + longTrendKernel + shortTrendKernel

        self.radiationGP = GPy.models.GPRegression(time, obs, kernel)

        self.radiationGP.optimize(messages=True)

        if(self.futureData is None):
            self.initializeFuture()
            
        res = self.radiationGP.predict(self.histData['Fractional Year'].values[:,None])
        self.histData['Mean Net Radiation [MJ/m^2/day]'] = res[0]
        self.histData['StdDev Net Radiation [MJ/m^2/day]'] = np.sqrt(res[1])
        
        res = self.radiationGP.predict(self.futureData['Fractional Year'].values[:,None])
        self.futureData['Mean Net Radiation [MJ/m^2/day]'] = res[0]
        self.futureData['StdDev Net Radiation [MJ/m^2/day]'] = np.sqrt(res[1])
        
        # Get the full covariance matrix and do a Cholesky factorization for future use in preditctions. 
        S   = self.radiationGP.predict(self.futureData['Fractional Year'].values[:,None],full_cov=True)  
        L   = np.linalg.cholesky(S[1])
        self.radiationMu   = S[0]
        self.radiationSigL = L

        
    def trainTemperature(self):
        thinBy = 4
        
        obs = self.histData['Air Temperature [C]'].iloc[0::thinBy].values
        time = self.histData['Fractional Year'].iloc[0::thinBy]

        # Remove NaN's from the data because that will cause GPy to act up
        time = time[~np.isnan(obs)][:,None]
        obs = obs[~np.isnan(obs)][:,None]

        # Define each part of the covariance kernels
        periodKernel = (GPy.kern.PeriodicMatern32(input_dim=1, variance=1.0, lengthscale=0.1, period=1)
                            * GPy.kern.RBF(input_dim=1, variance=10., lengthscale=5.0) )
        longTrendKernel = GPy.kern.RBF(input_dim=1, variance=1., lengthscale=7.0)
        shortTrendKernel = GPy.kern.RBF(input_dim=1, variance=0.2, lengthscale=0.1)

        # Combine the kernels
        kernel = periodKernel + longTrendKernel + shortTrendKernel

        self.tempGP = GPy.models.GPRegression(time,obs,kernel)
        self.tempGP.sum.mul.periodic_Matern32.period.fix()
       
        lPrior = GPy.priors.gamma_from_EV(0.2, 0.1)
        self.tempGP.sum.rbf_1.lengthscale.set_prior(lPrior, warning=False)

        noisePrior = GPy.priors.gamma_from_EV(0.08, 0.05)
        self.tempGP.Gaussian_noise.variance.set_prior(noisePrior, warning=False)
        
        lPrior = GPy.priors.gamma_from_EV(0.2, 0.1) # (mean, variance) push these larger. 
        self.tempGP.sum.rbf_1.lengthscale.set_prior(lPrior, warning=False)

        noisePrior = GPy.priors.gamma_from_EV(15.0, 25.0)
        self.tempGP.Gaussian_noise.variance.set_prior(noisePrior, warning=False)
        
        self.tempGP.optimize(messages=True)
        
        if(self.futureData is None):
            self.initializeFuture()
            
        res = self.tempGP.predict(self.histData['Fractional Year'].values[:,None])
        self.histData['Mean Air Temperature [C]'] = res[0]
        self.histData['StdDev Air Temperature [C]'] = np.sqrt(res[1])
        
        res = self.tempGP.predict(self.futureData['Fractional Year'].values[:,None])
        self.futureData['Mean Air Temperature [C]'] = res[0]
        self.futureData['StdDev Air Temperature [C]'] = np.sqrt(res[1])
        
        # Get the full covariance matrix and do a Cholesky factorization for future use in preditctions. 
        S   = self.tempGP.predict(self.futureData['Fractional Year'].values[:,None],full_cov=True)  
        L   = np.linalg.cholesky(S[1])
        self.tempMu   = S[0]
        self.tempSigL = L
        
        
    def trainWind(self):
        
        thinBy = 3
        
        obs = self.histData['Wind Speed [m/s]']
        
        obsMu = obs.rolling(50,center=True,min_periods=1).mean().iloc[0::thinBy].values
        time = self.histData['Fractional Year'].iloc[0::thinBy]

        # Remove NaN's from the data because that will cause GPy to act up
        time   = time[~np.isnan(obsMu)][:,None]
        obsMu = obsMu[~np.isnan(obsMu)][:,None]

        # Define each part of the covariance kernels
        periodKernel = ( GPy.kern.PeriodicMatern32(input_dim=1, variance=2.0, lengthscale=0.5, period=1)
                        * GPy.kern.RBF(input_dim=1, variance=2.0, lengthscale=6.0) )
        longTrendKernel = GPy.kern.RBF(input_dim=1, variance=2, lengthscale=8.0)
        biasKernel = GPy.kern.Bias(input_dim=1)
        #shortTrendKernel = GPy.kern.RBF(input_dim=1, variance=0.1, lengthscale=0.1)

        # Combine the kernels
        kernel = periodKernel + longTrendKernel + biasKernel

        self.windMuGP = GPy.models.GPRegression(time,obsMu,kernel)

        self.windMuGP.Gaussian_noise.variance = 0.2*0.2
        self.windMuGP.Gaussian_noise.variance.fix()
        self.windMuGP.sum.mul.rbf.lengthscale.fix()
        self.windMuGP.sum.mul.periodic_Matern32.period.fix()
        self.windMuGP.sum.rbf.variance.fix()
        
        self.windMuGP.optimize(messages=True)

        obsVar = obs.rolling(50,center=True,min_periods=1).var().iloc[0::thinBy].values
        time = np.arange(0,obsVar.shape[0]*thinBy,thinBy)/365 

        # Remove NaN's from the data because that will cause GPy to act up
        time   = time[~np.isnan(obsVar)][:,None]
        obsVar = obsVar[~np.isnan(obsVar)][:,None]

        # Define each part of the covariance kernels
        periodKernel = (GPy.kern.PeriodicMatern32(input_dim=1, variance=1.0, lengthscale=0.5, period=1)
                        * GPy.kern.RBF(input_dim=1, variance=1., lengthscale=6.0))
        longTrendKernel = GPy.kern.RBF(input_dim=1, variance=0.5, lengthscale=8.0)
        biasKernel = GPy.kern.Bias(input_dim=1)
        #shortTrendKernel = GPy.kern.RBF(input_dim=1, variance=0.1, lengthscale=0.1)

        # Combine the kernels
        kernel = periodKernel + longTrendKernel + biasKernel

        # Fit the variance GP to the log of the data so that predictions of the variance itself will be positive \exp(\log(\sigma^2)) > 0
        self.windVarGP = GPy.models.GPRegression(time,np.log(obsVar),kernel)

        self.windVarGP.Gaussian_noise.variance = 0.3*0.3
        self.windVarGP.Gaussian_noise.variance.fix()
        self.windVarGP.sum.mul.rbf.lengthscale.fix()
        self.windVarGP.sum.mul.periodic_Matern32.period.fix()
        self.windVarGP.optimize(messages=True)
        
        # We keep the gamma values found below for use in the random futures to be created later. 
        futureWindMu  = self.windMuGP.predict(self.futureData['Fractional Year'].values[:,None])[0]
        
        # The Gaussian process was fit on the log of the variance, so here we need to exponentiate the result to obtain the variance.
        # We use this exponentiation trick to avoid negative values
        futureWindVar = np.exp(self.windVarGP.predict(self.futureData['Fractional Year'].values[:,None])[0])

        self.gamma_a = futureWindMu*futureWindMu/futureWindVar
        self.gamma_b = futureWindMu/futureWindVar

    def trainPressure(self):
        
        thinBy = 4
        
        obs = self.histData['Vapor Pressure [kPa]'].iloc[0::thinBy].values
        time = self.histData['Fractional Year'].iloc[0::thinBy]

        # Remove NaN's from the data because that will cause GPy to act up
        time = time[~np.isnan(obs)][:,None]
        obs = obs[~np.isnan(obs)][:,None]

        # Define each part of the covariance kernels
        periodKernel = ( GPy.kern.PeriodicMatern32(input_dim=1, variance=1.0, lengthscale=0.1, period=1)
                         * GPy.kern.RBF(input_dim=1, variance=10., lengthscale=5.0) )
        longTrendKernel = GPy.kern.RBF(input_dim=1, variance=1., lengthscale=7.0)
        shortTrendKernel = GPy.kern.RBF(input_dim=1, variance=0.2, lengthscale=0.1)

        # Combine the kernels
        kernel = periodKernel + longTrendKernel + shortTrendKernel

        self.vaporGP = GPy.models.GPRegression(time[0::4],np.log(obs[0::4]),kernel)
        self.vaporGP.sum.mul.periodic_Matern32.period.fix()

        self.vaporGP.optimize(messages=True)
        
        if(self.futureData is None):
            self.initializeFuture()
            
        res = self.vaporGP.predict(self.histData['Fractional Year'].values[:,None])
        self.histData['Mean Vapor Pressure [kPa]'] = res[0]
        self.histData['StdDev Vapor Pressure [kPa]'] = np.sqrt(res[1])
        
        res = self.vaporGP.predict(self.futureData['Fractional Year'].values[:,None])
        self.futureData['Mean Vapor Pressure [kPa]'] = res[0]
        self.futureData['StdDev Vapor Pressure [kPa]'] = np.sqrt(res[1])
        
        # Get the full covariance matrix and do a Cholesky factorization for future use in preditctions. 
        S   = self.vaporGP.predict(self.futureData['Fractional Year'].values[:,None],full_cov=True)  
        L   = np.linalg.cholesky(S[1])
        self.vaporMu   = S[0]
        self.vaporSigL = L
        
        
    def trainPrecip(self):
        
        thinBy = 3
        
        precip = self.histData['Precipitation [ft]'].iloc[0::thinBy].values
        obs    =  precip > 1e-13
        time = self.histData['Fractional Year'].iloc[0::thinBy]

        self.precipVals = precip[obs]

        # Remove NaN's from the data because that will cause GPy to act up
        time = time[~np.isnan(obs)][:,None]
        obs  = np.array(obs[~np.isnan(obs)][:,None], dtype=np.float)

        periodKernel    = ( GPy.kern.PeriodicMatern32(input_dim=1, variance=1.0, lengthscale=0.5, period=1)
                           * GPy.kern.RBF(input_dim=1, variance=1., lengthscale=5.0) )
        longTrendKernel = GPy.kern.RBF(input_dim=1, variance=1., lengthscale=7.0)

        # Combine the kernels
        kernel = periodKernel + longTrendKernel
        lik    = GPy.likelihoods.Bernoulli()

        self.rainEventGP = GPy.core.GP(X=time,
                                       Y=obs, 
                                       kernel=kernel, 
                                       inference_method=GPy.inference.latent_function_inference.expectation_propagation.EP(),
                                       likelihood=lik)

        self.rainEventGP.sum.mul.periodic_Matern32.period.fix()
        
        self.rainEventGP.optimize('bfgs', max_iters=1000,messages=True)
        
        # Get the full covariance matrix and do a Cholesky factorization for future use in preditctions. 
        S   = self.rainEventGP.predict_noiseless(self.futureData['Fractional Year'].values[:,None],full_cov=True) 
        L   = np.linalg.cholesky(S[1]+(1e-10)*np.eye(len(S[0])))
        self.rainEventMu   = S[0]
        self.rainEventSigL = L
            
    #=========================================
    # ESTABLISH A SAMPLE FUTURE ENVIRONMENT 
    #=========================================
    
    def initializeFuture(self):
        
        futureTime = self.histData['Fractional Year'].iloc[-1] + np.linspace(0,self.futureYears, self.futureYears*365)
        self.futureData = pd.DataFrame({'Fractional Year': futureTime})
        
    def sampleRadiation(self):
        
        if(self.futureData is None):
            self.initializeFuture()
        
        z = np.random.randn( len(self.futureData['Fractional Year'].values[:,None]),1 )
        self.futureData['Net Radiation [MJ/m^2/day]'] = self.radiationMu + np.dot(self.radiationSigL,z)       
    
    def sampleTemperature(self):
        
        if(self.futureData is None):
            self.initializeFuture()

        z = np.random.randn( len(self.futureData['Fractional Year'].values[:,None]),1 )
        self.futureData['Air Temperature [C]'] = self.tempMu + np.dot(self.tempSigL,z)  
        
    def sampleWind(self):
        
        if(self.futureData is None):
            self.initializeFuture()
                
        self.futureData['Wind Speed [m/s]'] = np.array([gamma.rvs(self.gamma_a[i], loc=0, scale=1.0/self.gamma_b[i], 
                                             size=1)[0] for i in range(self.gamma_a.shape[0])])
        

    def samplePressure(self):
        
        if(self.futureData is None):
            self.initializeFuture()
            
        z = np.random.randn(len(self.futureData['Fractional Year'].values[:,None]),1 )
        self.futureData['Vapor Pressure [kPa]'] = np.exp(self.vaporMu + np.dot(self.vaporSigL,z)).ravel()      

        
    def samplePrecip(self):
        
        if(self.futureData is None):
            self.initializeFuture()
                
        z = np.random.randn(len(self.futureData['Fractional Year'].values[:,None]),1 )
        fsim = self.rainEventMu + np.dot(self.rainEventSigL,z) 
        
        futurePrecipEvents = self.rainEventGP.likelihood.samples(fsim).ravel()        
        futurePrecipEvents = futurePrecipEvents.transpose()

        self.futureData['Precipitation [ft]'] = futurePrecipEvents*self.precipVals[np.random.randint(self.precipVals.shape[0],size=futurePrecipEvents.shape[0])]
 
        
    #===============================
    # PLOT Future DATA
    #===============================
        
    def plotTrainedRadiation(self, ax=None, SaveName=None):
        
        if(ax is None):
            fig, ax = plt.subplots()
            
        #display(self.radiationGP)
        
        ax.plot(self.histData['Fractional Year'], self.histData['Net Radiation [MJ/m^2/day]'], '.k',label='Observed')
        ax.plot(self.histData['Fractional Year'], self.histData['Mean Net Radiation [MJ/m^2/day]'].values)
        ax.fill_between(self.histData['Fractional Year'], 
                        self.histData['Mean Net Radiation [MJ/m^2/day]'].values - 2.0*self.histData['StdDev Net Radiation [MJ/m^2/day]'].values, 
                        self.histData['Mean Net Radiation [MJ/m^2/day]'].values + 2.0*self.histData['StdDev Net Radiation [MJ/m^2/day]'].values)
        
        #self.sampleRadiation()
        
        ax.plot(self.futureData['Fractional Year'], self.futureData['Net Radiation [MJ/m^2/day]'], '.b',label='Predicted')
        
        ax.plot(self.futureData['Fractional Year'], self.futureData['Mean Net Radiation [MJ/m^2/day]'].values)
        ax.fill_between(self.futureData['Fractional Year'], 
                        self.futureData['Mean Net Radiation [MJ/m^2/day]'] - 2.0*self.futureData['StdDev Net Radiation [MJ/m^2/day]'].values, 
                        self.futureData['Mean Net Radiation [MJ/m^2/day]'] + 2.0*self.futureData['StdDev Net Radiation [MJ/m^2/day]'].values)
        
        ax.set_xlabel('Time [yrs]')
        ax.set_ylabel('Net Radiation [MJ/m^2/day]')
        
        if(SaveName!=None):
            plt.savefig(SaveName)
        
    def plotTrainedPrecip(self, ax=None, SaveName=None):
        
        if(ax is None):
            fig, ax = plt.subplots()
            
        #display(self.rainEventGP)
        
        ax.plot(self.histData['Fractional Year'], self.histData['Precipitation [ft]'], '.k',label='Observed')

        #self.samplePrecip()
        
        ax.plot(self.futureData['Fractional Year'], self.futureData['Precipitation [ft]'], '.b',label='Predicted')
        
        ax.set_xlabel('Time [yrs]')
        ax.set_ylabel('Precipitation [ft]')
        
        if(SaveName!=None):
            plt.savefig(SaveName)
        
        
    def plotTrainedTemperature(self, ax=None, SaveName=None):
                               
        if(ax is None):
            fig, ax = plt.subplots()
            
        ax.plot(self.histData['Fractional Year'], self.histData['Air Temperature [C]'], '.k',label='Observed')
        ax.plot(self.histData['Fractional Year'], self.histData['Mean Air Temperature [C]'].values)
        ax.fill_between(self.histData['Fractional Year'], 
                        self.histData['Mean Air Temperature [C]'].values - 2.0*self.histData['StdDev Air Temperature [C]'].values, 
                        self.histData['Mean Air Temperature [C]'].values + 2.0*self.histData['StdDev Air Temperature [C]'].values)
        
        #self.sampleTemperature()
        
        ax.plot(self.futureData['Fractional Year'], self.futureData['Air Temperature [C]'],'.b',label='Predicted')
        ax.plot(self.futureData['Fractional Year'], self.futureData['Mean Air Temperature [C]'].values)
        ax.fill_between(self.futureData['Fractional Year'], 
                        self.futureData['Mean Air Temperature [C]'].values - 2.0*self.futureData['StdDev Air Temperature [C]'].values, 
                        self.futureData['Mean Air Temperature [C]'].values + 2.0*self.futureData['StdDev Air Temperature [C]'].values)
        
        ax.set_xlabel('Time [yrs]')
        ax.set_ylabel('Air Temperature [Celsius]')
        
        if(SaveName!=None):
            plt.savefig(SaveName)
        
    def plotTrainedWind(self, ax=None, SaveName=None):
        
        if(ax is None):
            fig, ax = plt.subplots()
            
        ax.plot(self.histData['Fractional Year'], self.histData['Wind Speed [m/s]'], '.k',label='Observed')
        
        #self.sampleWind()
        
        ax.plot(self.futureData['Fractional Year'], self.futureData['Wind Speed [m/s]'],'.b',label='Predicted')
        
        ax.set_xlabel('Time [yrs]')
        ax.set_ylabel('Wind Speed [m/s]')
        
        if(SaveName!=None):
            plt.savefig(SaveName)
        
        
    def plotTrainedPressure(self, ax= None, SaveName=None):
        
        if(ax is None):
            fig, ax = plt.subplots()
            
        ax.plot(self.histData['Fractional Year'], self.histData['Vapor Pressure [kPa]'], '.k',label='Observed')
        ax.plot(self.histData['Fractional Year'], np.exp(self.histData['Mean Vapor Pressure [kPa]'].values))
        ax.fill_between(self.histData['Fractional Year'], 
                        np.exp(self.histData['Mean Vapor Pressure [kPa]'] - self.histData['StdDev Vapor Pressure [kPa]']), 
                        np.exp(self.histData['Mean Vapor Pressure [kPa]'] + self.histData['StdDev Vapor Pressure [kPa]']))
        
        #self.samplePressure()
        
        ax.plot(self.futureData['Fractional Year'], self.futureData['Vapor Pressure [kPa]'],'.b',label='Predicted')
        ax.plot(self.futureData['Fractional Year'], np.exp(self.futureData['Mean Vapor Pressure [kPa]']).values, label='$\exp[\mu]$')
        ax.fill_between(self.futureData['Fractional Year'], 
                        np.exp(self.futureData['Mean Vapor Pressure [kPa]'].values - 2.0*self.futureData['StdDev Vapor Pressure [kPa]'].values), 
                        np.exp(self.futureData['Mean Vapor Pressure [kPa]'].values + 2.0*self.futureData['StdDev Vapor Pressure [kPa]'].values), label='$\exp[\mu\pm\sigma]$')
        
        ax.set_xlabel('Time [yrs]')
        ax.set_ylabel('Vapor Pressure [kPa]')
        
        if(SaveName!=None):
            plt.savefig(SaveName)
            
    def plotTrainedDataStacked(self,SaveName=None):
        # Looks at all the trained data in a stacked plot
        # Set up the plotting. 
        fit, ax1 = plt.subplots(5,figsize=(10,17.5),sharex=True)
        
        # Radiation:
        ax1[0].plot(self.histData['Fractional Year'], self.histData['Net Radiation [MJ/m^2/day]'], '.k',label='Observed')
        ax1[0].plot(self.histData['Fractional Year'], self.histData['Mean Net Radiation [MJ/m^2/day]'].values)
        ax1[0].fill_between(self.histData['Fractional Year'], 
                        self.histData['Mean Net Radiation [MJ/m^2/day]'].values 
                        - 2.0*self.histData['StdDev Net Radiation [MJ/m^2/day]'].values, 
                        self.histData['Mean Net Radiation [MJ/m^2/day]'].values 
                        + 2.0*self.histData['StdDev Net Radiation [MJ/m^2/day]'].values)
        
        ax1[0].plot(self.futureData['Fractional Year'], self.futureData['Net Radiation [MJ/m^2/day]'], '.b',label='Predicted')
        
        ax1[0].plot(self.futureData['Fractional Year'], self.futureData['Mean Net Radiation [MJ/m^2/day]'].values)
        ax1[0].fill_between(self.futureData['Fractional Year'], 
                        self.futureData['Mean Net Radiation [MJ/m^2/day]'] 
                        - 2.0*self.futureData['StdDev Net Radiation [MJ/m^2/day]'].values, 
                        self.futureData['Mean Net Radiation [MJ/m^2/day]'] 
                        + 2.0*self.futureData['StdDev Net Radiation [MJ/m^2/day]'].values)
        ax1[0].set_ylim(bottom=-8.0,top=27.0)
        ax1[0].set_ylabel('Net Radiation [MJ/$m^2$/day]', fontsize=14)
        
        # Precipitation:
        ax1[1].plot(self.histData['Fractional Year'], self.histData['Precipitation [ft]'], '.k',label='Observed')        
        ax1[1].plot(self.futureData['Fractional Year'], self.futureData['Precipitation [ft]'], '.b',label='Predicted')
        ax1[1].set_ylim(bottom=0.0,top=0.014)
        ax1[1].set_ylabel('Precipitation [ft]', fontsize=14)
        
        # Temperature:
        ax1[2].plot(self.histData['Fractional Year'], self.histData['Air Temperature [C]'], '.k',label='Observed')
        ax1[2].plot(self.histData['Fractional Year'], self.histData['Mean Air Temperature [C]'].values)
        ax1[2].fill_between(self.histData['Fractional Year'], 
                        self.histData['Mean Air Temperature [C]'].values - 2.0*self.histData['StdDev Air Temperature [C]'].values, 
                        self.histData['Mean Air Temperature [C]'].values + 2.0*self.histData['StdDev Air Temperature [C]'].values)
        
        ax1[2].plot(self.futureData['Fractional Year'], self.futureData['Air Temperature [C]'],'.b',label='Predicted')
        ax1[2].plot(self.futureData['Fractional Year'], self.futureData['Mean Air Temperature [C]'].values)
        ax1[2].fill_between(self.futureData['Fractional Year'], 
                        self.futureData['Mean Air Temperature [C]'].values - 2.0*self.futureData['StdDev Air Temperature [C]'].values, 
                        self.futureData['Mean Air Temperature [C]'].values + 2.0*self.futureData['StdDev Air Temperature [C]'].values)
        ax1[2].set_ylim(bottom=0.0,top=34.0)
        ax1[2].set_ylabel('Air Temperature [Celsius]', fontsize=14)
        
        # Pressure:
        ax1[3].plot(self.histData['Fractional Year'], self.histData['Vapor Pressure [kPa]'], '.k',label='Observed')
        ax1[3].plot(self.histData['Fractional Year'], np.exp(self.histData['Mean Vapor Pressure [kPa]'].values))
        ax1[3].fill_between(self.histData['Fractional Year'], 
                        np.exp(self.histData['Mean Vapor Pressure [kPa]'] - self.histData['StdDev Vapor Pressure [kPa]']), 
                        np.exp(self.histData['Mean Vapor Pressure [kPa]'] + self.histData['StdDev Vapor Pressure [kPa]']))
        
        
        ax1[3].plot(self.futureData['Fractional Year'], self.futureData['Vapor Pressure [kPa]'],'.b',label='Predicted')
        ax1[3].plot(self.futureData['Fractional Year'], np.exp(self.futureData['Mean Vapor Pressure [kPa]']).values,
                    label='$\exp[\mu]$')
        ax1[3].fill_between(self.futureData['Fractional Year'], 
                        np.exp(self.futureData['Mean Vapor Pressure [kPa]'].values 
                               - 2.0*self.futureData['StdDev Vapor Pressure [kPa]'].values), 
                        np.exp(self.futureData['Mean Vapor Pressure [kPa]'].values 
                               + 2.0*self.futureData['StdDev Vapor Pressure [kPa]'].values), label='$\exp[\mu\pm\sigma]$')
        ax1[3].set_ylim(bottom=0.0,top=4.0)
        ax1[3].set_ylabel('Vapor Pressure [kPa]', fontsize=14)
        
        # Wind Speed:
        ax1[4].plot(self.histData['Fractional Year'], self.histData['Wind Speed [m/s]'], '.k',label='Observed')
        ax1[4].plot(self.futureData['Fractional Year'], self.futureData['Wind Speed [m/s]'],'.b',label='Predicted')
        ax1[4].set_xlabel('Years', fontsize=16)
        ax1[4].set_ylim(bottom=0.0,top=5.0)
        ax1[4].set_ylabel('Wind Speed [m/s]', fontsize=14)
        
        if(SaveName!=None):
            plt.savefig(SaveName+".pdf", bbox_inches='tight')
        
        
    def plotTrainedData(self,SaveName=None):
        # Looks at sample or trained data to show some fun plots. 
        print("   Plots of Trained Data:")
        if(SaveName!=None):
            SaveName = "TrainedRadiationPlot.pdf"
        self.plotTrainedRadiation(SaveName=SaveName)
        if(SaveName!=None):
            SaveName = "TrainedTemperaturePlot.pdf"
        self.plotTrainedTemperature(SaveName=SaveName)
        if(SaveName!=None):
            SaveName = "TrainedWindPlot.pdf"        
        self.plotTrainedWind(SaveName=SaveName)
        if(SaveName!=None):
            SaveName = "TrainedPressurePlot.pdf"        
        self.plotTrainedPressure(SaveName=SaveName)
        if(SaveName!=None):
            SaveName = "TrainedPrecipPlot.pdf"
        self.plotTrainedPrecip(SaveName=SaveName)
    
    #====================================
    # PLOT PREDICTED MEAN AND VARIANCE
    #====================================
    
    def plotPredictedMuVarWind(self,ax=None,SaveName=None):
        # Plot an instance of the predicted wind mean and variance over time.
        if(ax is None):
            fig, ax = plt.subplots()

        thinBy = 1
        
        futureWindMu_mean, futureWindMu_var   = self.windMuGP.predict(self.futureData['Fractional Year'].values[::thinBy,None])
        futureWindVar_mean, futureWindVar_var = self.windVarGP.predict(self.futureData['Fractional Year'].values[::thinBy,None])
        
        xpts = np.linspace(0, len(futureWindMu_mean)/365, len(futureWindMu_mean))

        fwmp = futureWindMu_mean[:,0] + np.sqrt(futureWindMu_var)[:,0]
        fwmm = futureWindMu_mean[:,0] - np.sqrt(futureWindMu_var)[:,0]
        fwvp = np.exp(futureWindVar_mean[:,0] + np.sqrt(futureWindVar_var)[:,0])
        fwvm = np.exp(futureWindVar_mean[:,0] - np.sqrt(futureWindVar_var)[:,0])
        

        ax.fill_between(thinBy*xpts, fwmp, fwmm, color='k',alpha=0.2,  label='$\mu\pm\sigma$')
        ax.plot(thinBy*xpts,futureWindMu_mean,'k',label='forcasted $\mu$')
    
        ax.fill_between(thinBy*xpts, fwvp, fwvm, color='r',alpha=0.2,  label='$\exp(\mu\pm\sigma)$')
        ax.plot(thinBy*xpts,np.exp(futureWindVar_mean),'r',label='forcasted $\sigma^2$')

        ax.set_xlabel('Years')
        ax.set_ylabel('wind [m/s]')
        ax.legend(loc=1)
        
        if(SaveName!=None):
            plt.savefig(SaveName)

    def plotAirTempAndPrecip(self,SaveName=None):
        # This is a postprocess plot to visualize some of the relationships in the training data.     
        keepNum  = 24*7
        years    = len(self.histData['Precipitation [in]'])/365
        xPrecip  = np.linspace(0, years, len(self.histData['Precipitation [in]']))
        xAirTemp = np.linspace(0, years, len(self.histData['Air Temperature [F]']))

        fig, ax1 = plt.subplots()
        ax1.plot(xPrecip,self.histData['Precipitation [in]'], label='Precipitation')
        ax1.legend(loc=1)
        ax1.set_xlabel('Years')
        ax1.set_ylabel('Precipitation [in]')

        ax2 = ax1.twinx()
        ax2.plot(xAirTemp,self.histData['Air Temperature [F]'].values,'-g', label='Air Temperature')
        ax2.legend(loc=2)
        ax2.set_ylabel('Air Temperature [F]')
        
        if(SaveName!=None):
            plt.savefig(SaveName)
        
    def plotRefET0andPrecipitation(self,SaveName=None):
        years = len(self.histData['Precipitation [in]'])/365
        ft2mm = 304.8 #mm/ft
        in2mm = 25.4  #mm/in

        xET0  = np.linspace(0, years, len(self.histData['Reference ET0 [ft]']))
        fig, ax1 = plt.subplots()
        xPrecip  = np.linspace(0, years, len(self.histData['Precipitation [in]']))

        ax1.plot(xPrecip,self.histData['Precipitation [in]']*in2mm, label='Precipitation')
        ax1.legend(loc=1)
        ax1.set_xlabel('Years')
        ax1.set_ylabel('Precipitation [mm]')
        ax2 = ax1.twinx()

        ax2.plot(xPrecip,self.histData['Reference ET0 [ft]']*ft2mm,'r',label='Reference ET0')
        ax2.legend(loc=2)
        ax2.set_ylabel('Reference ET0 [mm]')
        
        if(SaveName!=None):
            plt.savefig(SaveName)
    
    def plotPrecipitationAndWindSpeed(self,SaveName=None):
        keepNum = 24*7
        years   = len(self.histData['Precipitation [in]'])/365
        xPrecip    = np.linspace(0, years, len(self.histData['Precipitation [in]']))
        xWindSpeed = np.linspace(0, years, len(self.histData['Wind Speed [m/s]']))

        fig, ax1 = plt.subplots()
        ax1.plot(xPrecip,self.histData['Precipitation [in]'], label='Precipitation')
        ax1.legend(loc=1)
        ax1.set_xlabel('Years')
        ax1.set_ylabel('Precipitation [in]')

        ax2 = ax1.twinx()
        ax2.plot(xWindSpeed,self.histData['Wind Speed [m/s]'].values,'-g',alpha=0.4, label='Wind Speed')
        ax2.legend(loc=2)
        ax2.set_ylabel('Wind Speed [m/s]')
        
        if(SaveName!=None):
            plt.savefig(SaveName)        
        
    def plotWindSpeeVsPrecipitation(self,SaveName=None):
        fig, ax1 = plt.subplots()
        ax1.plot(self.histData['Wind Speed [m/s]'],self.histData['Precipitation [in]'],'r.')
        ax1.legend(loc=1)
        ax1.set_xlabel('Wind Speed [m/s]')
        ax1.set_ylabel('Precipitation [in]')

        if(SaveName!=None):
            plt.savefig(SaveName)        
      
    def plotSolarRadiationAndVaporPressure(self,SaveName=None):
        years  = len(self.histData['Solar Radiation [Ly/day]'])/365
        xSolar = np.linspace(0, years, len(self.histData['Solar Radiation [Ly/day]']))
        xVapor = np.linspace(0, years, len(self.histData['Vapor Pressure [mBars]']))
        fig, ax1 = plt.subplots()
        ax1.plot(xSolar,self.histData['Solar Radiation [Ly/day]'], label='Solar Radiation')
        ax1.legend(loc=1)
        ax1.set_xlabel('Years')
        ax1.set_ylabel('Radiation [Ly/day]')

        ax2 = ax1.twinx()
        ax2.plot(xVapor,self.histData['Vapor Pressure [mBars]'].values,'-g', label='Vapor Pressure')
        ax2.legend(loc=2)
        ax2.set_ylabel('Pressure [mBars]')
        
        if(SaveName!=None):
            plt.savefig(SaveName)        