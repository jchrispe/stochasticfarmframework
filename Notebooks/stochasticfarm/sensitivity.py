import numpy as np

def isnotebook():
    """ Function returns true if code is being executed in IPython environment.
        
        Adapted from https://stackoverflow.com/questions/15411967/how-can-i-check-if-code-is-executed-in-the-ipython-notebook
    """
    try:
        shell = get_ipython().__class__.__name__
        if shell == 'ZMQInteractiveShell':
            return True   # Jupyter notebook or qtconsole
        elif shell == 'TerminalInteractiveShell':
            return False  # Terminal running IPython
        else:
            return False  # Other type (?)
    except NameError:
        return False      # Probably standard Python interpreter
    
    
if isnotebook():
    from tqdm.notebook import tqdm
else:
    from tqdm import tqdm
    

def GlobalSensitivity(samples, model):
    """
    Computes the main and total effect indices for a model $f(x)$.  Uses the 

    ARGUMENTS:
        samples (list of list) : Samples of the input parameters grouped by input "blocks".   
                                 For example, if $f(x)=f(x_1,x_2,x_3)$ with $x_1\in \mathbb{R}^{N_1}$, 
                                 $x_2\in \mathbb{R}^{N_2}$, and $x_3\in \mathbb{R}^{N_3}$, then this 
                                 array will contain samples of the joint distribution $p(x_1,x_2,x_3)$. 
                                 `samples[i][j]` should contain a numpy array with sample $i$ of $x_j$.
        model (function) : A function that accepts a list of vector-valued inputs $[x_1,x_2,x_3]$ and returns 
                           a single vector-valued output $f(x_1,x_2,x_3)$.   

    RETURNS:
        mainEffects (list of np.array) : The main effects for each input.  mainEffects[i][j] contains the 
                                         sensitivity of output component $j$ with respect to input $x_i$.
        totalEffects (list of np.array) : The total effects for each input.  mainEffects[i][j] contains the 
                                          sensitivity of output component $j$ with respect to input $x_i$.

    """
    numSamps = len(samples)
    numGroups = len(samples[0])

    # Split the samples into two groups "A" and "B"
    halfNum = int( np.floor(0.5*numSamps))

    # Split into two groups
    A = samples[0:halfNum]
    B = samples[halfNum:2*halfNum]

    # Evaluate the model over A and B
    fA = []
    fB = []
    for i in tqdm(range(halfNum), 'Performing initial model evaluations'):
        fA.append( model(A[i]) )
        fB.append( model(B[i]) )
        
    fA = np.array(fA)
    fB = np.array(fB)

    # Compute the mean and variance of f
    meanF = 0.5*(np.mean(fA,axis=0) + np.mean(fB,axis=0))
    varF = (1.0/(numSamps-1.0))*(np.sum((fA-meanF)**2,axis=0) + np.sum((fB-meanF)**2,axis=0))
    
    # For each group, compute fAB and the main effect estimator
    mainEffects = [None]*numGroups
    totalEffects = [None]*numGroups

    for groupInd in range(numGroups):
       
        mainSum = np.zeros(fA[0].shape)
        totalSum = np.zeros(fA[0].shape)

        for sampInd in tqdm(range(halfNum),'Computing indices for group {}/{}'.format(groupInd+1,numGroups)):
            
            # Create the model input, setting all inputs from A except for the group index, which is from B
            samp = [a for a in A[sampInd]]
            samp[groupInd] = B[sampInd][groupInd]

            # Evaluate the model at this mixed input
            fAB = model(samp)

            # Compute the contributions to the main effects and total effects
            mainSum += fB[sampInd]*(fAB - fA[sampInd]) # fB[sampInd]*(fAB - fA[sampInd])
            totalSum += (fA[sampInd]- fAB)**2
        
        mainEffects[groupInd] = (mainSum / halfNum) / varF #1.0 - (mainSum / (2.0*halfNum)) / varF
        totalEffects[groupInd] = (totalSum / (2.0*halfNum) ) / varF

    return mainEffects, totalEffects



if __name__=='__main__':

    def TestModel(inputs):
        x = inputs[0]
        y = inputs[1]

        return y*x**2

    numSamps = 100000
    samps = [None]*numSamps
    for i in range(numSamps):
        x = np.random.randn(2)
        y = np.random.rand(2)
        samps[i] = [x,y]

    mainEffects, totalEffects = GlobalSensitivity(samps, TestModel)
    print(mainEffects)
    print(totalEffects) 