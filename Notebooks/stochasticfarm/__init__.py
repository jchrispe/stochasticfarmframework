from math import *
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import GPy
from IPython.display import display
from scipy.stats import gamma, gaussian_kde
from . import farm
from . import crop
from . import environment
from .sensitivity import *

class ApplicationContext:
    def __init__(self,futureYears):
        self.farm        = farm.farm("Dummy Name", 0.0)
        self.environment = environment.environment(futureYears=futureYears) 
        self.simYears    = futureYears
        self.samples     = 1             # Stochastic samples to get for predictive runs. 
        
    def displayProperties(self,SaveName=None):
        self.farm.displayFarm()
        self.farm.displayCrops()
        self.farm.displayGrowingSeasons()
        self.environment.displayData(SaveName)
        
    def updateFarmParameters(self,MaxIPerDay,WaterCost,AcrePerCrop,samples):
        self.farm.setMaxIrrigationPerDay(MaxIPerDay)
        self.farm.setWaterCost(WaterCost)
        self.farm.updateAcreage(AcrePerCrop)
        self.samples = samples
        
    def displayFarmParameters(self):
        self.farm.displayFarm()
        self.farm.displayCrops()
            
    def getCropYield(self,irrigation='Limited', crops=None, randThetaUni=None):
        """ Computes the yields of each crop under the current future data.
            
            ARGUMENTS:
                irrigation (string) : Defines the type of irrigation under consideration.  
                                      Valid options are 'Limited', 'NoIrrigation', or 'Unlimited'.
                crop (int or list of ints) : Specifies one or more crops to consider in the yield calculation.
                randThetaUni (float) : A number between [0,1] that sets the initial root zone depletion.  A value of 0 
                                       sets it to ThetaWP, a value of 1 results in ThetaFC
                
            RETURNS:
                cropValues (list of floats) : A list containing the cumulative yield for 
                                              each crop under the given irrigation scenario.
        """
        
        cropYields = []
        
        if(randThetaUni is None):
            randThetaUni = np.random.rand()
        
        if(crops is None):
            portfolio = self.farm.cropPortfolio
        elif(isinstance(crops,int)):
            portfolio = [self.farm.cropPortfolio[crops]]
        else:
            portfolio = [self.farm.cropPortfolio[crop] for crop in crops]
            
        # Under the limited water irrigation. 
        for crop in portfolio:
           
            lim, noi, ful = crop.Yield(self.environment.futureET0,
                                       self.environment.futureData['Precipitation [ft]'],
                                       self.farm.ThetaFC,
                                       self.farm.ThetaWP,
                                       self.farm.WaterCost,
                                       randThetaUni)
            
            if(irrigation=='Limited'):
                cropYields.append( np.sum(lim) )
            elif(irrigation=='NoIrrigation'):
                cropYields.append( np.sum(noi) )
            elif(irrigation=='Unlimited'):
                cropYields.append( np.sum(ful) )
                
        return cropYields
    
            
    def plotFutureCropValues(self,SaveName=None):
        
        # Under the limited water irrigation. 
        for crop in range(len(self.farm.cropPortfolio)):
            
            # Yearly Display
            #xptsCropValue = np.linspace(0,len(self.environment.futureData['Precipitation [ft]'])/365,
            #                            len(self.environment.futureData['Precipitation [ft]']))
            # Daily Display
            
            years = len(self.environment.futureData['Precipitation [ft]'])/365
            xptsCropValue = np.linspace(0,365*years,len(self.environment.futureData['Precipitation [ft]']))
            
            self.farm.cropPortfolio[crop].CropValue(self.environment.futureET0,self.environment.futureData['Precipitation [ft]'],
                                        self.farm.ThetaFC,self.farm.ThetaWP,self.farm.WaterCost)

            cropValuelim    = self.farm.cropPortfolio[crop].CropValue_lim
            cumCropValuelim = cropValuelim.cumsum()
            cropValuenoi    = self.farm.cropPortfolio[crop].CropValue_noi
            cumCropValuenoi = cropValuenoi.cumsum()
            cropValueful    = self.farm.cropPortfolio[crop].CropValue_ful
            cumCropValueful = cropValueful.cumsum()
            
            # Add a flag here to deal with value as yield if crop value is $1.0/metric ton. 
            if self.farm.cropPortfolio[crop].value == 1.0: 
                fit, ax1 = plt.subplots(2,figsize=(10,6), sharex=True)
                # plotting as yield in metric tons. 
                ax1[0].plot(xptsCropValue,cropValuelim,'k:',label='Limited Irrigation')
                ax1[0].plot(xptsCropValue,cropValuenoi,'r',label='No Irrigation')
                ax1[0].plot(xptsCropValue,cropValueful,'m-',label='Unlimited Irrigation')
                if(self.farm.cropPortfolio[crop].name == "Strawberry"):
                    ax1[0].set_ylim(bottom=0.0,top=0.025)
                    ax1[1].set_ylim(bottom=0.0,top=5.0)
                ax1[0].set_ylabel(self.farm.cropPortfolio[crop].name+'\n Predicted Yield/Day \n Metric Tons ')
                
            
                ax1[1].plot(xptsCropValue,cumCropValuelim,'k:',label='Limited Irrigation')
                ax1[1].plot(xptsCropValue,cumCropValuenoi,'r',label='No Irrigation')
                ax1[1].plot(xptsCropValue,cumCropValueful,'m-',label='Unlimited Irrigation')
                
                ax1[1].set_ylabel(self.farm.cropPortfolio[crop].name+'\n Aggregate Yield \n Metric Tons')
            

                # Put a legend below current axis
                #ax1[0].legend(loc='upper left', ncol=1, fancybox=True, shadow=True)
                ax1[1].legend(loc='upper left', ncol=1, fancybox=True, shadow=True)
                ax1[1].set_xlabel('Days')
                if(SaveName!=None):
                    plt.savefig(SaveName+"Yield"+self.farm.cropPortfolio[crop].name+".pdf", bbox_inches='tight')
                    
                    
            else:
                
                fit, ax1 = plt.subplots(figsize=(10,5), sharex=True)
                # plotting value given the non-trivial value of the crop. 
                ax1.plot(xptsCropValue,cropValuelim,'k:',label='Value ' + self.farm.cropPortfolio[crop].name + ' limited Irrigation')
                ax1.plot(xptsCropValue,cropValuenoi,'m--',label='Value ' + self.farm.cropPortfolio[crop].name + ' no Irrigation')
                ax1.plot(xptsCropValue,cropValueful,'c',label='Value ' + self.farm.cropPortfolio[crop].name + ' unlimited Irrigation')
          
                ax1.set_ylabel('Crop Value in \$ '+self.farm.cropPortfolio[crop].name)
                ax1.set_xlabel('Years')
            
                ax2 = ax1.twinx()
                ax2.plot(xptsCropValue,cumCropValuelim,'b-',label='Cumulative Crop Value ' + self.farm.cropPortfolio[crop].name + ' limited Irrigation')
                ax2.plot(xptsCropValue,cumCropValuenoi,'r-.',label='Cumulative Crop Value ' + self.farm.cropPortfolio[crop].name + ' no Irrigation')
                ax2.plot(xptsCropValue,cumCropValueful,'g:',label='Cumulative Crop Value ' + self.farm.cropPortfolio[crop].name + ' unlimited Irrigation')
                ax2.legend(loc=2)
                ax2.set_ylabel('Cumulative Crop Value in \$ ' + self.farm.cropPortfolio[crop].name)
            
                # Shrink current axis's height by 10% on the bottom
                box = ax1.get_position()
                ax1.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
                ax2.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])

                # Put a legend below current axis
                ax1.legend(loc='upper left', bbox_to_anchor=(0.55, -0.06),fancybox=True, shadow=True, ncol=1)
                ax2.legend(loc='upper right', bbox_to_anchor=(0.45, -0.06),fancybox=True, shadow=True, ncol=1)
                if(SaveName!=None):
                    plt.savefig(SaveName+"Value"+self.farm.cropPortfolio[crop].name+".pdf", bbox_inches='tight')


            
    def plotRootZoneWaterUse(self,SaveName=None):

        for crop in range(len(self.farm.cropPortfolio)):

            # RAW (Readily available soil water in rootzone [mm])
            RAW  = self.farm.cropPortfolio[crop].p*self.farm.cropPortfolio[crop].TAW 
            ft2mm = 304.8
            npts = len(self.environment.futureData['Precipitation [ft]'])
            time = np.linspace(0,len(self.environment.futureData['Precipitation [ft]'])/365, npts)
            print(self.farm.cropPortfolio[crop].name,
                  "Crops are stressed when root zone depleation is bigger than: ", RAW, " [mm].")

            # Set up the plotting. 
            fit, ax1 = plt.subplots(4,figsize=(10,12),sharex=True,gridspec_kw={'height_ratios': [1, 1, 1, 0.5]})
            plt.title('Precipitation and Irrigation Impact on Root Zone Depletion')
            
            # ET
            ax1[0].plot(time,self.environment.futureET0,'g', alpha=0.5,label="$ET_0$ (Forecasted)") 
            ax1[0].plot(time,self.farm.cropPortfolio[crop].ETa_lim,'k:' ,alpha=0.5,label='$ET_a$ limited Irrigation')
            ax1[0].plot(time,self.farm.cropPortfolio[crop].ETa_noi,'r'  ,alpha=0.5,label='$ET_a$ no Irrigation')
            ax1[0].plot(time,self.farm.cropPortfolio[crop].ETa_ful,'m--',alpha=0.5,label='$ET_a$ unlimited Irrigation')
            ax1[0].set_ylabel(self.farm.cropPortfolio[crop].name + ' $ET_a$ [mm]' )
            ax1[0].legend(loc='upper left', ncol=1, fancybox=True, shadow=True)
            
            # Above Ground
            ax1[1].plot(time,self.farm.cropPortfolio[crop].I_lim,'k',alpha=0.5,label='Limited Irrigation')
            ax1[1].plot(time,self.farm.cropPortfolio[crop].I_ful,'m--',alpha=0.5,label='Unlimited Irrigation')
            ax1[1].set_ylabel(self.farm.cropPortfolio[crop].name + ' Irrigation [mm]')
            ax1[1].set_ylim(bottom=0.0,top=max(self.farm.cropPortfolio[crop].I_ful)*1.2)
            ax1[1].legend(loc='upper left', ncol=1, fancybox=True, shadow=True)
        
            # Root-zone
            ax1[2].plot(time,self.farm.cropPortfolio[crop].Dr_lim,'k:',label='$D_r$ limited Irrigation')
            ax1[2].plot(time,self.farm.cropPortfolio[crop].Dr_noi,'r',label='$D_r$ no Irrigation')
            ax1[2].plot(time,self.farm.cropPortfolio[crop].Dr_ful,'m--',label='$D_r$ unlimited Irrigation')
            ax1[2].plot(time,np.ones(npts)*RAW,'y',alpha=0.5,label='$RAW$ (root zone)')
            ax1[2].set_ylabel(self.farm.cropPortfolio[crop].name + ' Root Zone [mm]')
            ax1[2].set_ylim(bottom=0.0,top=max(self.farm.cropPortfolio[crop].Dr_noi)*1.1)
            ax1[2].legend(loc='upper left',ncol=1, fancybox=True, shadow=True)
            
            ax1[3].set_ylabel('Precipitation [mm] \n (Forecasted)')                                                                   
            ax1[3].plot(time,ft2mm*self.environment.futureData['Precipitation [ft]'],'b',alpha=0.8,label='Precipitation (Forecasted)')
            ax1[3].set_xlabel('Years', fontsize=14)
            
            if(SaveName!=None):
                plt.savefig(SaveName+self.farm.cropPortfolio[crop].name+".pdf", bbox_inches='tight')


    def plotIrrigationCostAndPrecipitation(self,SaveName=None):        
        for crop in range(len(self.farm.cropPortfolio)):
            fit, ax1 = plt.subplots(figsize=(10,5))
            ft2mm = 304.8
            npts  = len(self.environment.futureData['Precipitation [ft]'])
            time  = np.linspace(0,len(self.environment.futureData['Precipitation [ft]'])/365, npts)

            ax1.plot(time,ft2mm*self.environment.futureData['Precipitation [ft]'],'b',alpha=0.8,label='Precipitation [mm]')
            ax1.set_xlabel('Years')
            ax1.legend(loc=1)
            ax1.set_ylim(bottom=0.0)
            ax1.set_ylabel('Precipitation in [mm]')
            ax2 = ax1.twinx()
            ax2.plot(time,self.farm.cropPortfolio[crop].ICost_lim,'k:',alpha=0.5,
                     label='limited Irrigation : water cost for ' +self.farm.cropPortfolio[crop].name)
            ax2.plot(time,self.farm.cropPortfolio[crop].ICost_noi,'ro',alpha=0.5,
                     label='no Irrigation : water cost for ' +self.farm.cropPortfolio[crop].name)
            ax2.plot(time,self.farm.cropPortfolio[crop].ICost_ful,'m--',alpha=0.5,
                     label='unlimited Irrigation : water cost for ' +self.farm.cropPortfolio[crop].name)
            ax2.legend(loc=2)
            ax2.set_ylim(bottom=0.0)
            ax2.set_ylabel('Irrigation Cost in [$] over time for '+self.farm.cropPortfolio[crop].name)
            
            if(SaveName!=None):
                plt.savefig(SaveName+self.farm.cropPortfolio[crop].name+".pdf", bbox_inches='tight')
            
            
    def plotCumIrrigationCostAndCumPrecipitation(self,SaveName=None):        
        for crop in range(len(self.farm.cropPortfolio)):
            fit, ax1 = plt.subplots(figsize=(10,5))
            ft2mm = 304.8
            npts  = len(self.environment.futureData['Precipitation [ft]'])
            time  = np.linspace(0,len(self.environment.futureData['Precipitation [ft]'])/365, npts)
            
            cumCropIrrCostlim = self.farm.cropPortfolio[crop].ICost_lim.cumsum()
            cumCropIrrCostnoi = self.farm.cropPortfolio[crop].ICost_noi.cumsum()
            cumCropIrrCostful = self.farm.cropPortfolio[crop].ICost_ful.cumsum()
            cumPrecip         = ft2mm*self.environment.futureData['Precipitation [ft]'].cumsum()
            
            ax1.plot(time,cumPrecip,'b',alpha=0.8,label='Cumulative Precipitation [mm]')
            ax1.set_xlabel('Years')
            ax1.legend(loc=1)
            ax1.set_ylim(bottom=0.0)
            ax1.set_ylabel('Cumulative precipitation in [mm]')
            ax2 = ax1.twinx()
            ax2.plot(time,cumCropIrrCostlim,'k:',alpha=0.5,
                     label='limited Irrigation : water cost for ' +self.farm.cropPortfolio[crop].name)
            ax2.plot(time,cumCropIrrCostnoi,'ro',alpha=0.5,
                     label='no Irrigation : water cost for ' +self.farm.cropPortfolio[crop].name)
            ax2.plot(time,cumCropIrrCostful,'m--',alpha=0.5,
                     label='unlimited Irrigation : water cost for ' +self.farm.cropPortfolio[crop].name)
            ax2.legend(loc=2)
            ax2.set_ylim(bottom=0.0)
            ax2.set_ylabel('Cumulative Irrigation Cost in [$] over time for '+self.farm.cropPortfolio[crop].name)
            
            if(SaveName!=None):
                plt.savefig(SaveName+self.farm.cropPortfolio[crop].name+".pdf", bbox_inches='tight')
                        
            
    def plotETaAndRootZoneDepleation(self,SaveName=None):
        for crop in range(len(self.farm.cropPortfolio)):
            fit, ax1 = plt.subplots(figsize=(10,5))
            time = np.linspace(0,len(self.environment.futureData['Precipitation [ft]'])/365,
                               len(self.environment.futureData['Precipitation [ft]']))
            ax1.plot(time,self.farm.cropPortfolio[crop].Dr_lim,label='Root Zone Depletion : limited Irrigation')
            ax1.plot(time,self.farm.cropPortfolio[crop].Dr_noi,label='Root Zone Depletion : no Irrigation')
            ax1.plot(time,self.farm.cropPortfolio[crop].Dr_ful,label='Root Zone Depletion : unlimited Irrigation')
            
            ax1.set_xlabel('Years')
            ax1.legend(loc=1)
            ax1.set_ylabel('Root Zone Depleation [mm] for '+ self.farm.cropPortfolio[crop].name)
            ax2 = ax1.twinx()
            ax2.plot(time,self.farm.cropPortfolio[crop].ETa_lim,'k:',alpha=0.5,
                     label='ETa [mm] for ' + self.farm.cropPortfolio[crop].name + ' : limited Irrigation')
            ax2.plot(time,self.farm.cropPortfolio[crop].ETa_noi,'r',alpha=0.5,
                     label='ETa [mm] for ' + self.farm.cropPortfolio[crop].name + ' : no Irrigation')
            ax2.plot(time,self.farm.cropPortfolio[crop].ETa_ful,'m--',alpha=0.5,
                     label='ETa [mm] for ' + self.farm.cropPortfolio[crop].name + ' : unlimited Irrigation')
            ax2.legend(loc=2)
            ax2.set_ylabel('ETa [mm] over time for ' + self.farm.cropPortfolio[crop].name)
            
            # Shrink current axis's height by 10% on the bottom
            box = ax1.get_position()
            ax1.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
            ax2.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])

            # Put a legend below current axis
            ax1.legend(loc='upper left', bbox_to_anchor=(0.55, -0.06),fancybox=True, shadow=True, ncol=1)
            ax2.legend(loc='upper right', bbox_to_anchor=(0.45, -0.06),fancybox=True, shadow=True, ncol=1)
            
            if(SaveName!=None):
                plt.savefig(SaveName+self.farm.cropPortfolio[crop].name+".pdf", bbox_inches='tight')
            
    def plotCropValuesAndWaterCost(self,SaveName=None):
        for crop in range(len(self.farm.cropPortfolio)):
            fit, ax1 = plt.subplots(figsize=(10,5))
            xptsCropValue = np.linspace(0,len(self.environment.futureData['Precipitation [ft]'])/365,
                                        len(self.environment.futureData['Precipitation [ft]']))
            self.farm.cropPortfolio[crop].CropValue(
                                        self.environment.futureET0,self.environment.futureData['Precipitation [ft]'],
                                        self.farm.ThetaFC,self.farm.ThetaWP,self.farm.WaterCost)
            
            cumCropValue_lim = self.farm.cropPortfolio[crop].CropValue_lim.cumsum()
            cumCropValue_noi = self.farm.cropPortfolio[crop].CropValue_noi.cumsum()
            cumCropValue_ful = self.farm.cropPortfolio[crop].CropValue_ful.cumsum()   
            
            waterCost_lim    = self.farm.cropPortfolio[crop].ICost_lim
            waterCost_noi    = self.farm.cropPortfolio[crop].ICost_noi
            waterCost_ful    = self.farm.cropPortfolio[crop].ICost_ful

            waterCostSum_lim  = waterCost_lim.cumsum()
            waterCostSum_noi  = waterCost_noi.cumsum()
            waterCostSum_ful  = waterCost_ful.cumsum()
            
            ax1.plot(xptsCropValue,self.farm.cropPortfolio[crop].CropValue_lim,'k',
                     label='Crop Value '+self.farm.cropPortfolio[crop].name + ' : limited Irrigation')
            ax1.plot(xptsCropValue,waterCost_lim,'k--',label='Water Cost : limited Irrigation')
            ax1.plot(xptsCropValue,self.farm.cropPortfolio[crop].CropValue_noi,'r',
                     label='Crop Value '+self.farm.cropPortfolio[crop].name + ' : no Irrigation')
            ax1.plot(xptsCropValue,waterCost_noi,'m--',label='Water Cost : no Irrigation')
            ax1.plot(xptsCropValue,self.farm.cropPortfolio[crop].CropValue_ful,'m',
                     label='Crop Value '+self.farm.cropPortfolio[crop].name + ' : unlimited Irrigation')
            ax1.plot(xptsCropValue,waterCost_ful,'c--',label='Water Cost : unlimited Irrigation')
            ax1.set_ylabel('Dollars [\$] '+self.farm.cropPortfolio[crop].name)
            ax1.set_xlabel('Years')
            ax1.legend(loc=1)
            
            ax2 = ax1.twinx()
            ax2.plot(xptsCropValue,cumCropValue_lim,'k:',label='Cumulative Value ' + self.farm.cropPortfolio[crop].name + ' : limited Irrigaiton')
            ax2.plot(xptsCropValue,waterCostSum_lim,'k',label='Cumulative Cost ' + self.farm.cropPortfolio[crop].name + ' : limited Irrigation')
            
            ax2.plot(xptsCropValue,cumCropValue_noi,'m-',label='Cumulative Value ' + self.farm.cropPortfolio[crop].name + ' : no Irrigaiton')
            ax2.plot(xptsCropValue,waterCostSum_noi,'m',label='Cumulative Cost ' + self.farm.cropPortfolio[crop].name + ' : no Irrigation')
            
            ax2.plot(xptsCropValue,cumCropValue_ful,'c:',label='Cumulative Value ' + self.farm.cropPortfolio[crop].name + ' : unlimited Irrigaiton')
            ax2.plot(xptsCropValue,waterCostSum_ful,'c',label='Cumulative Cost ' + self.farm.cropPortfolio[crop].name + ' : unlimited Irrigation')
            
            ax2.set_ylabel('Cumulative Water Cost [\$] for ' + self.farm.cropPortfolio[crop].name)
            ax2.legend(loc=2)
            
            # Shrink current axis's height by 10% on the bottom
            box = ax1.get_position()
            ax1.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
            ax2.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])

            # Put a legend below current axis
            ax1.legend(loc='upper left', bbox_to_anchor=(0.55, -0.06),fancybox=True, shadow=True, ncol=1)
            ax2.legend(loc='upper right', bbox_to_anchor=(0.45, -0.06),fancybox=True, shadow=True, ncol=1)
            
            if(SaveName!=None):
                plt.savefig(SaveName+self.farm.cropPortfolio[crop].name+".pdf", bbox_inches='tight')
            
            
    def plotET0InstanceAndCumulativeET0(self,SaveName=None):
        # Single sample of ET0 
        fig, ax1 = plt.subplots(figsize=(10,5))
        xptsET0  = np.linspace(0, len(self.environment.futureET0)/365, len(self.environment.futureET0))
        cumET0   = self.environment.futureET0.cumsum()
        ax1.plot(xptsET0,self.environment.futureET0,'k',label='$ET_0$')
        ax1.set_xlabel('Years')
        ax1.set_ylabel('$ET_0$')
        ax2 = ax1.twinx()
        ax2.plot(xptsET0,cumET0,'r',label='Cumulative $ET_0$')
        ax2.legend(loc=2)
        ax2.set_ylabel('Cumulative $ET_0$')  
        
        if(SaveName!=None):
                plt.savefig(SaveName+".pdf", bbox_inches='tight')
           
    def plotCumulativeET0Sampled(self,samples,SaveName=None):        
        # Several samples of ET0 over time (plot hard coded into notebook above):
        fig, ax1 = plt.subplots(figsize=(10,5))
        xptsET0  = np.linspace(0, len(self.environment.futureET0)/365, len(self.environment.futureET0))
        ax1.set_xlabel('Years')
        ax1.set_ylabel('Cumulative $ET_0$')

        for i in range(samples):
            self.environment.sample()
            cumET0   = self.environment.futureET0.cumsum()
            ax1.plot(xptsET0,cumET0,color='k',alpha=0.1,label='Cumulative $ET_0$')
        if(SaveName!=None):
                plt.savefig(SaveName+'samples'+str(samples)+".pdf", bbox_inches='tight')
                
    def GenerateSampledDataFrame(self,samples,SaveName=None):
        # Set values needed for the data frame. 
        ncrops              = len(self.farm.cropPortfolio)
        futureYears         = int(len(self.environment.futureET0)/365)
        
        self.cumWaterCostSamples_lim = np.zeros((samples,ncrops*futureYears))
        self.cumWaterCostSamples_noi = np.zeros((samples,ncrops*futureYears))
        self.cumWaterCostSamples_ful = np.zeros((samples,ncrops*futureYears))
        
        self.cumCropValueSamples_lim = np.zeros((samples,ncrops*futureYears))
        self.cumCropValueSamples_noi = np.zeros((samples,ncrops*futureYears))
        self.cumCropValueSamples_ful = np.zeros((samples,ncrops*futureYears))
        
        self.cumET0Samples       = np.zeros((samples,futureYears))
        # Set some values for plotting
        fig, ax1 = plt.subplots(figsize=(14,7))
        xpts     = np.linspace(0, futureYears, len(self.environment.futureET0))
        colormap = plt.cm.gist_ncar 
        colors   = [colormap(i) for i in np.linspace(0.0, 0.9,6*ncrops)]       

        # Take samples to populate the data frame. 
        for mySample in range(samples):
            self.environment.sample()
            # Get cumulative sum of water cost per crop.
            cumET0   = self.environment.futureET0.cumsum()
            for crop in range(ncrops):
                self.farm.cropPortfolio[crop].CropValue(self.environment.futureET0,
                                                             self.environment.futureData['Precipitation [ft]'],
                                                             self.farm.ThetaFC,self.farm.ThetaWP,self.farm.WaterCost)
                
                cropValue_lim = self.farm.cropPortfolio[crop].CropValue_lim
                cropValue_noi = self.farm.cropPortfolio[crop].CropValue_noi
                cropValue_ful = self.farm.cropPortfolio[crop].CropValue_ful
                
                waterCost_lim = self.farm.cropPortfolio[crop].ICost_lim
                waterCost_noi = self.farm.cropPortfolio[crop].ICost_noi
                waterCost_ful = self.farm.cropPortfolio[crop].ICost_ful
                
                cumWaterCost_lim = waterCost_lim.cumsum()
                cumCropValue_lim = cropValue_lim.cumsum()
                
                cumWaterCost_noi = waterCost_noi.cumsum()
                cumCropValue_noi = cropValue_noi.cumsum()
                
                cumWaterCost_ful = waterCost_ful.cumsum()
                cumCropValue_ful = cropValue_ful.cumsum()
        
                for year in range(futureYears):
                    if(year == 0):
                        #print("year = ",year, " Sanity Check")
                        #print("adding at : [",mySample," ,",crop*(futureYears)+year,"]")
                        self.cumET0Samples[mySample][year] = cumET0[365*(year+1)-1]    
                        
                        self.cumWaterCostSamples_lim[mySample][crop*(futureYears)+year] = cumWaterCost_lim[365*(year+1)-1] 
                        self.cumWaterCostSamples_noi[mySample][crop*(futureYears)+year] = cumWaterCost_noi[365*(year+1)-1] 
                        self.cumWaterCostSamples_ful[mySample][crop*(futureYears)+year] = cumWaterCost_ful[365*(year+1)-1]
                        
                        self.cumCropValueSamples_lim[mySample][crop*(futureYears)+year] = cumCropValue_lim[365*(year+1)-1] 
                        self.cumCropValueSamples_noi[mySample][crop*(futureYears)+year] = cumCropValue_noi[365*(year+1)-1] 
                        self.cumCropValueSamples_ful[mySample][crop*(futureYears)+year] = cumCropValue_ful[365*(year+1)-1]
                    else:
                        #print("year = ",year, " Sanity Check")
                        #print("adding at : [",mySample," ,",crop*(futureYears)+year,"]")
                        self.cumET0Samples[mySample][year] = cumET0[365*(year+1)-1] - cumET0[365*(year)-1] 
                        
                        self.cumWaterCostSamples_lim[mySample][crop*(futureYears)+year] = (cumWaterCost_lim[365*(year+1)-1] 
                                                                                  - cumWaterCost_lim[365*(year)-1])
                        self.cumWaterCostSamples_noi[mySample][crop*(futureYears)+year] = (cumWaterCost_noi[365*(year+1)-1] 
                                                                                  - cumWaterCost_noi[365*(year)-1])
                        self.cumWaterCostSamples_ful[mySample][crop*(futureYears)+year] = (cumWaterCost_ful[365*(year+1)-1] 
                                                                                  - cumWaterCost_ful[365*(year)-1])
                        
                        self.cumCropValueSamples_lim[mySample][crop*(futureYears)+year] = (cumCropValue_lim[365*(year+1)-1] 
                                                                                  - cumCropValue_lim[365*(year)-1])
                        self.cumCropValueSamples_noi[mySample][crop*(futureYears)+year] = (cumCropValue_noi[365*(year+1)-1] 
                                                                                  - cumCropValue_noi[365*(year)-1])
                        self.cumCropValueSamples_ful[mySample][crop*(futureYears)+year] = (cumCropValue_ful[365*(year+1)-1] 
                                                                                  - cumCropValue_ful[365*(year)-1])
                        
                #===========================
                # Add a fun plot to the mix.
                #===========================

                if(mySample == 0):
                    ax1.plot(xpts,cumWaterCost_lim,color=colors[6*crop  ],alpha=1,
                             label='Cumulative Water Cost '+self.farm.cropPortfolio[crop].name + ' : limited Irrigation')
                    ax1.plot(xpts,cumCropValue_lim,color=colors[6*crop+1],alpha=1,
                             label='Cumulative Crop Value '+self.farm.cropPortfolio[crop].name + ' : limited Irrigation')
                    ax1.plot(xpts,cumWaterCost_noi,color=colors[6*crop+2],alpha=1,
                             label='Cumulative Water Cost '+self.farm.cropPortfolio[crop].name + ' : no Irrigation')
                    ax1.plot(xpts,cumCropValue_noi,color=colors[6*crop+3],alpha=1,
                             label='Cumulative Crop Value '+self.farm.cropPortfolio[crop].name + ' : no Irrigation')
                    ax1.plot(xpts,cumWaterCost_ful,color=colors[6*crop+4],alpha=1,
                             label='Cumulative Water Cost '+self.farm.cropPortfolio[crop].name + ' : unlimited Irrigation')
                    ax1.plot(xpts,cumCropValue_ful,color=colors[6*crop+5],alpha=1,
                             label='Cumulative Crop Value '+self.farm.cropPortfolio[crop].name + ' : unlimited Irrigation')                    
                if(mySample > 0):
                    ax1.plot(xpts,cumWaterCost_lim,color=colors[6*crop  ],alpha=0.1)
                    ax1.plot(xpts,cumCropValue_lim,color=colors[6*crop+1],alpha=0.1)
                    ax1.plot(xpts,cumWaterCost_noi,color=colors[6*crop+2],alpha=0.1)
                    ax1.plot(xpts,cumCropValue_noi,color=colors[6*crop+3],alpha=0.1)
                    ax1.plot(xpts,cumWaterCost_ful,color=colors[6*crop+4],alpha=0.1)
                    ax1.plot(xpts,cumCropValue_ful,color=colors[6*crop+5],alpha=0.1)
        
        ax1.legend(loc=2)
        ax1.set_xlabel('Years')
        ax1.set_ylabel('Dollars [\$]')
        
        # Shrink current axis's height by 10% on the bottom
        box = ax1.get_position()
        ax1.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
        
        # Put a legend below current axis
        ax1.legend(loc='upper left', bbox_to_anchor=(0.1, -0.11),fancybox=True, shadow=True, ncol=2)
        
        if(SaveName!=None):
                plt.savefig(SaveName+str(samples)+'Samples.pdf', bbox_inches='tight')
                
    def GenerateSampledDataFrameUniformWaterPrice(self,samples,SaveName=None,waterPts=10):
        # Set values needed for the data frame. 
        ncrops              = len(self.farm.cropPortfolio)
        futureYears         = int(len(self.environment.futureET0)/365)
        
        # Set a vector of uniform water prices between a specified minimum and maximum. 
        waterPriceVec       = np.linspace(self.farm.MinWaterCost,self.farm.MaxWaterCost,waterPts)
        
        self.cumWaterCostSamples_lim = np.zeros((samples*waterPts,ncrops*futureYears))
        self.cumWaterCostSamples_noi = np.zeros((samples*waterPts,ncrops*futureYears))
        self.cumWaterCostSamples_ful = np.zeros((samples*waterPts,ncrops*futureYears))
        
        self.cumCropValueSamples_lim = np.zeros((samples*waterPts,ncrops*futureYears))
        self.cumCropValueSamples_noi = np.zeros((samples*waterPts,ncrops*futureYears))
        self.cumCropValueSamples_ful = np.zeros((samples*waterPts,ncrops*futureYears))
        
        self.cumCropYieldSamples_lim = np.zeros((samples*waterPts,ncrops*futureYears))
        self.cumCropYieldSamples_noi = np.zeros((samples*waterPts,ncrops*futureYears))
        self.cumCropYieldSamples_ful = np.zeros((samples*waterPts,ncrops*futureYears))
        
        self.cumET0Samples           = np.zeros((samples,futureYears))
        
        # Set some values for plotting
        fig, ax1 = plt.subplots(figsize=(14,7))
        xpts     = np.linspace(0, futureYears, len(self.environment.futureET0))
        colormap = plt.cm.gist_ncar 
        colors   = [colormap(i) for i in np.linspace(0.0, 0.9,6*ncrops)]       

        # Take samples to populate the data frame. 
        #for mySample in range(samples):
        for mySample in tqdm(range(samples), 'Generating Environment Samples'):
            self.environment.sample()
            # Get cumulative sum of water cost per crop.
            cumET0   = self.environment.futureET0.cumsum()
            # Add level for price:
            for pindex, price in enumerate(waterPriceVec):
                for crop in range(ncrops):
                    
                    cropYield_lim, cropYield_noi, cropYield_ful = self.farm.cropPortfolio[crop].Yield(self.environment.futureET0,
                                                                               self.environment.futureData['Precipitation [ft]'],
                                                                               self.farm.ThetaFC,self.farm.ThetaWP,
                                                                               self.farm.WaterCost)
                    
                    cropValue_lim = self.farm.cropPortfolio[crop].area*self.farm.cropPortfolio[crop].value*cropYield_lim 
                    cropValue_noi = self.farm.cropPortfolio[crop].area*self.farm.cropPortfolio[crop].value*cropYield_noi
                    cropValue_ful = self.farm.cropPortfolio[crop].area*self.farm.cropPortfolio[crop].value*cropYield_ful
                    
                    # This bit will be redundant from the above for a minute as Yiled is called in CropValue.
                    #self.farm.cropPortfolio[crop].CropValue(self.environment.futureET0,
                    #                                             self.environment.futureData['Precipitation [ft]'],
                    #                                             self.farm.ThetaFC,self.farm.ThetaWP,price) 
                
                    #cropValue_lim = self.farm.cropPortfolio[crop].CropValue_lim
                    #cropValue_noi = self.farm.cropPortfolio[crop].CropValue_noi
                    #cropValue_ful = self.farm.cropPortfolio[crop].CropValue_ful
                    
                    waterCost_lim = self.farm.cropPortfolio[crop].ICost_lim
                    waterCost_noi = self.farm.cropPortfolio[crop].ICost_noi
                    waterCost_ful = self.farm.cropPortfolio[crop].ICost_ful
                
                    cumCropYield_lim = cropYield_lim.cumsum()
                    cumWaterCost_lim = waterCost_lim.cumsum()
                    cumCropValue_lim = cropValue_lim.cumsum()
                
                    cumCropYield_noi = cropYield_noi.cumsum()
                    cumWaterCost_noi = waterCost_noi.cumsum()
                    cumCropValue_noi = cropValue_noi.cumsum()
                
                    cumCropYield_ful = cropYield_ful.cumsum()
                    cumWaterCost_ful = waterCost_ful.cumsum()
                    cumCropValue_ful = cropValue_ful.cumsum()
        
                    for year in range(futureYears):
                        if(year == 0):
                            #print("year = ",year, " Sanity Check")
                            #print("adding at : [",mySample," ,",crop*(futureYears)+year,"]")
                            self.cumET0Samples[mySample][year] = cumET0[365*(year+1)-1] 
                            
                            self.cumCropYieldSamples_lim[mySample*waterPts+pindex][crop*(futureYears)+year] = cumCropYield_lim[365*(year+1)-1] 
                            self.cumCropYieldSamples_noi[mySample*waterPts+pindex][crop*(futureYears)+year] = cumCropYield_noi[365*(year+1)-1] 
                            self.cumCropYieldSamples_ful[mySample*waterPts+pindex][crop*(futureYears)+year] = cumCropYield_ful[365*(year+1)-1]
                        
                            self.cumWaterCostSamples_lim[mySample*waterPts+pindex][crop*(futureYears)+year] = cumWaterCost_lim[365*(year+1)-1] 
                            self.cumWaterCostSamples_noi[mySample*waterPts+pindex][crop*(futureYears)+year] = cumWaterCost_noi[365*(year+1)-1] 
                            self.cumWaterCostSamples_ful[mySample*waterPts+pindex][crop*(futureYears)+year] = cumWaterCost_ful[365*(year+1)-1]
                        
                            self.cumCropValueSamples_lim[mySample*waterPts+pindex][crop*(futureYears)+year] = cumCropValue_lim[365*(year+1)-1] 
                            self.cumCropValueSamples_noi[mySample*waterPts+pindex][crop*(futureYears)+year] = cumCropValue_noi[365*(year+1)-1] 
                            self.cumCropValueSamples_ful[mySample*waterPts+pindex][crop*(futureYears)+year] = cumCropValue_ful[365*(year+1)-1]
                        else:
                            #print("year = ",year, " Sanity Check")
                            #print("adding at : [",mySample," ,",crop*(futureYears)+year,"]")
                            self.cumET0Samples[mySample][year] = cumET0[365*(year+1)-1] - cumET0[365*(year)-1] 
                        
                            self.cumCropYieldSamples_lim[mySample*waterPts+pindex][crop*(futureYears)+year] = (cumCropYield_lim[365*(year+1)-1] 
                                                                                      - cumCropYield_lim[365*(year)-1])
                            self.cumCropYieldSamples_noi[mySample*waterPts+pindex][crop*(futureYears)+year] = (cumCropYield_noi[365*(year+1)-1] 
                                                                                      - cumCropYield_noi[365*(year)-1])
                            self.cumCropYieldSamples_ful[mySample*waterPts+pindex][crop*(futureYears)+year] = (cumCropYield_ful[365*(year+1)-1] 
                                                                                      - cumCropYield_ful[365*(year)-1])
                        
                            self.cumWaterCostSamples_lim[mySample*waterPts+pindex][crop*(futureYears)+year] = (cumWaterCost_lim[365*(year+1)-1] 
                                                                                      - cumWaterCost_lim[365*(year)-1])
                            self.cumWaterCostSamples_noi[mySample*waterPts+pindex][crop*(futureYears)+year] = (cumWaterCost_noi[365*(year+1)-1] 
                                                                                      - cumWaterCost_noi[365*(year)-1])
                            self.cumWaterCostSamples_ful[mySample*waterPts+pindex][crop*(futureYears)+year] = (cumWaterCost_ful[365*(year+1)-1] 
                                                                                      - cumWaterCost_ful[365*(year)-1])
                        
                            self.cumCropValueSamples_lim[mySample*waterPts+pindex][crop*(futureYears)+year] = (cumCropValue_lim[365*(year+1)-1] 
                                                                                      - cumCropValue_lim[365*(year)-1])
                            self.cumCropValueSamples_noi[mySample*waterPts+pindex][crop*(futureYears)+year] = (cumCropValue_noi[365*(year+1)-1] 
                                                                                      - cumCropValue_noi[365*(year)-1])
                            self.cumCropValueSamples_ful[mySample*waterPts+pindex][crop*(futureYears)+year] = (cumCropValue_ful[365*(year+1)-1] 
                                                                                      - cumCropValue_ful[365*(year)-1])
                        
                    #=====================================================
                    # Add a fun plot to the mix at maximum water price.
                    #=====================================================

                    if(mySample == 0 and price == self.farm.MaxWaterCost):
                        ax1.plot(xpts,cumWaterCost_lim,color=colors[6*crop  ],alpha=1,
                                 label='Cumulative Water Cost '+self.farm.cropPortfolio[crop].name + ' : limited Irrigation')
                        ax1.plot(xpts,cumCropValue_lim,color=colors[6*crop+1],alpha=1,
                                 label='Cumulative Crop Value '+self.farm.cropPortfolio[crop].name + ' : limited Irrigation')
                        ax1.plot(xpts,cumWaterCost_noi,color=colors[6*crop+2],alpha=1,
                                 label='Cumulative Water Cost '+self.farm.cropPortfolio[crop].name + ' : no Irrigation')
                        ax1.plot(xpts,cumCropValue_noi,color=colors[6*crop+3],alpha=1,
                                 label='Cumulative Crop Value '+self.farm.cropPortfolio[crop].name + ' : no Irrigation')
                        ax1.plot(xpts,cumWaterCost_ful,color=colors[6*crop+4],alpha=1,
                                 label='Cumulative Water Cost '+self.farm.cropPortfolio[crop].name + ' : unlimited Irrigation')
                        ax1.plot(xpts,cumCropValue_ful,color=colors[6*crop+5],alpha=1,
                                 label='Cumulative Crop Value '+self.farm.cropPortfolio[crop].name + ' : unlimited Irrigation')                    
                    if(mySample > 0):
                        ax1.plot(xpts,cumWaterCost_lim,color=colors[6*crop  ],alpha=0.1)
                        ax1.plot(xpts,cumCropValue_lim,color=colors[6*crop+1],alpha=0.1)
                        ax1.plot(xpts,cumWaterCost_noi,color=colors[6*crop+2],alpha=0.1)
                        ax1.plot(xpts,cumCropValue_noi,color=colors[6*crop+3],alpha=0.1)
                        ax1.plot(xpts,cumWaterCost_ful,color=colors[6*crop+4],alpha=0.1)
                        ax1.plot(xpts,cumCropValue_ful,color=colors[6*crop+5],alpha=0.1)
        
        ax1.legend(loc=2)
        ax1.set_xlabel('Years')
        ax1.set_ylabel('Dollars [\$]')
        
        # Shrink current axis's height by 10% on the bottom
        box = ax1.get_position()
        ax1.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
        
        # Put a legend below current axis
        ax1.legend(loc='upper left', bbox_to_anchor=(0.1, -0.11),fancybox=True, shadow=True, ncol=2)
        
        if(SaveName!=None):
                plt.savefig(SaveName+str(samples)+'Samples.pdf', bbox_inches='tight')
                
    def GenerateSampledDataFrameUniformMaxDailyWater(self,samples,MaxIPerDay,IrrigationPts=10):
        # Set values needed for the data frame. 
        ncrops              = len(self.farm.cropPortfolio)
        futureYears         = int(len(self.environment.futureET0)/365)
        
        # Set a vector of uniform water prices between a specified minimum and maximum. 
        if IrrigationPts == 1:
            maxIrrigationVec = [MaxIPerDay]
            print("Single Case: =", maxIrrigationVec)
        else:
            maxIrrigationVec = np.linspace(0,MaxIPerDay,IrrigationPts)
            print("Multi Points Case: =", maxIrrigationVec)
        
        self.cumWaterCostSamples_lim = np.zeros((samples*IrrigationPts,ncrops*futureYears))
        self.cumWaterCostSamples_noi = np.zeros((samples*IrrigationPts,ncrops*futureYears))
        self.cumWaterCostSamples_ful = np.zeros((samples*IrrigationPts,ncrops*futureYears))
        
        self.cumCropValueSamples_lim = np.zeros((samples*IrrigationPts,ncrops*futureYears))
        self.cumCropValueSamples_noi = np.zeros((samples*IrrigationPts,ncrops*futureYears))
        self.cumCropValueSamples_ful = np.zeros((samples*IrrigationPts,ncrops*futureYears))
        
        self.cumCropYieldSamples_lim = np.zeros((samples*IrrigationPts,ncrops*futureYears))
        self.cumCropYieldSamples_noi = np.zeros((samples*IrrigationPts,ncrops*futureYears))
        self.cumCropYieldSamples_ful = np.zeros((samples*IrrigationPts,ncrops*futureYears))
        
        self.cumET0Samples       = np.zeros((samples,futureYears))       

        # Take samples to populate the data frame. 
        #for mySample in range(samples):
        for mySample in tqdm(range(samples), 'Generating Environment Samples'):
            self.environment.sample()
            # Get cumulative sum of water cost per crop.
            cumET0   = self.environment.futureET0.cumsum()
            # Add level for price:
            for index, MaxIrr in enumerate(maxIrrigationVec):
                self.farm.setMaxIrrigationPerDay(MaxIrr) # Value is set across all crops Incoming value feet
                                                         # Value is converted to mm allowed per acre. 
                for crop in range(ncrops):
                    cropYield_lim, cropYield_noi, cropYield_ful = self.farm.cropPortfolio[crop].Yield(self.environment.futureET0,
                                                                               self.environment.futureData['Precipitation [ft]'],
                                                                               self.farm.ThetaFC,self.farm.ThetaWP,
                                                                               self.farm.WaterCost)
                    
                    cropValue_lim = self.farm.cropPortfolio[crop].area*self.farm.cropPortfolio[crop].value*cropYield_lim 
                    cropValue_noi = self.farm.cropPortfolio[crop].area*self.farm.cropPortfolio[crop].value*cropYield_noi
                    cropValue_ful = self.farm.cropPortfolio[crop].area*self.farm.cropPortfolio[crop].value*cropYield_ful
                    
                    # Turned off as this was redundant for a minute. 
                    #self.farm.cropPortfolio[crop].CropValue(self.environment.futureET0,
                    #                                        self.environment.futureData['Precipitation [ft]'],
                    #                                        self.farm.ThetaFC,self.farm.ThetaWP,
                    #                                        self.farm.WaterCost) 
                
                    #cropValue_lim = self.farm.cropPortfolio[crop].CropValue_lim
                    #cropValue_noi = self.farm.cropPortfolio[crop].CropValue_noi
                    #cropValue_ful = self.farm.cropPortfolio[crop].CropValue_ful
                    
                    waterCost_lim = self.farm.cropPortfolio[crop].ICost_lim
                    waterCost_noi = self.farm.cropPortfolio[crop].ICost_noi
                    waterCost_ful = self.farm.cropPortfolio[crop].ICost_ful
                
                    cumCropYield_lim = cropYield_lim.cumsum()
                    cumWaterCost_lim = waterCost_lim.cumsum()
                    cumCropValue_lim = cropValue_lim.cumsum()
                
                    cumCropYield_noi = cropYield_noi.cumsum()
                    cumWaterCost_noi = waterCost_noi.cumsum()
                    cumCropValue_noi = cropValue_noi.cumsum()
                
                    cumCropYield_ful = cropYield_ful.cumsum()
                    cumWaterCost_ful = waterCost_ful.cumsum()
                    cumCropValue_ful = cropValue_ful.cumsum()
        
                    for year in range(futureYears):
                        if(year == 0):
                            #print("year = ",year, " Sanity Check")
                            #print("adding at : [",mySample," ,",crop*(futureYears)+year,"]")
                            self.cumET0Samples[mySample][year] = cumET0[365*(year+1)-1]    

                            self.cumCropYieldSamples_lim[mySample*IrrigationPts+index][crop*(futureYears)+year] = cumCropYield_lim[365*(year+1)-1] 
                            self.cumCropYieldSamples_noi[mySample*IrrigationPts+index][crop*(futureYears)+year] = cumCropYield_noi[365*(year+1)-1] 
                            self.cumCropYieldSamples_ful[mySample*IrrigationPts+index][crop*(futureYears)+year] = cumCropYield_ful[365*(year+1)-1]
                            
                            self.cumWaterCostSamples_lim[mySample*IrrigationPts+index][crop*(futureYears)+year] = cumWaterCost_lim[365*(year+1)-1] 
                            self.cumWaterCostSamples_noi[mySample*IrrigationPts+index][crop*(futureYears)+year] = cumWaterCost_noi[365*(year+1)-1] 
                            self.cumWaterCostSamples_ful[mySample*IrrigationPts+index][crop*(futureYears)+year] = cumWaterCost_ful[365*(year+1)-1]
                        
                            self.cumCropValueSamples_lim[mySample*IrrigationPts+index][crop*(futureYears)+year] = cumCropValue_lim[365*(year+1)-1] 
                            self.cumCropValueSamples_noi[mySample*IrrigationPts+index][crop*(futureYears)+year] = cumCropValue_noi[365*(year+1)-1] 
                            self.cumCropValueSamples_ful[mySample*IrrigationPts+index][crop*(futureYears)+year] = cumCropValue_ful[365*(year+1)-1]
                        else:
                            #print("year = ",year, " Sanity Check")
                            #print("adding at : [",mySample," ,",crop*(futureYears)+year,"]")
                            self.cumET0Samples[mySample][year] = cumET0[365*(year+1)-1] - cumET0[365*(year)-1] 
                        

                            self.cumCropYieldSamples_lim[mySample*IrrigationPts+index][crop*(futureYears)+year] = (cumCropYield_lim[365*(year+1)-1] 
                                                                                      - cumCropYield_lim[365*(year)-1])
                            self.cumCropYieldSamples_noi[mySample*IrrigationPts+index][crop*(futureYears)+year] = (cumCropYield_noi[365*(year+1)-1] 
                                                                                      - cumCropYield_noi[365*(year)-1])
                            self.cumCropYieldSamples_ful[mySample*IrrigationPts+index][crop*(futureYears)+year] = (cumCropYield_ful[365*(year+1)-1] 
                                                                                      - cumCropYield_ful[365*(year)-1])
                        
                            self.cumWaterCostSamples_lim[mySample*IrrigationPts+index][crop*(futureYears)+year] = (cumWaterCost_lim[365*(year+1)-1] 
                                                                                      - cumWaterCost_lim[365*(year)-1])
                            self.cumWaterCostSamples_noi[mySample*IrrigationPts+index][crop*(futureYears)+year] = (cumWaterCost_noi[365*(year+1)-1] 
                                                                                      - cumWaterCost_noi[365*(year)-1])
                            self.cumWaterCostSamples_ful[mySample*IrrigationPts+index][crop*(futureYears)+year] = (cumWaterCost_ful[365*(year+1)-1] 
                                                                                      - cumWaterCost_ful[365*(year)-1])
                        
                            self.cumCropValueSamples_lim[mySample*IrrigationPts+index][crop*(futureYears)+year] = (cumCropValue_lim[365*(year+1)-1] 
                                                                                      - cumCropValue_lim[365*(year)-1])
                            self.cumCropValueSamples_noi[mySample*IrrigationPts+index][crop*(futureYears)+year] = (cumCropValue_noi[365*(year+1)-1] 
                                                                                      - cumCropValue_noi[365*(year)-1])
                            self.cumCropValueSamples_ful[mySample*IrrigationPts+index][crop*(futureYears)+year] = (cumCropValue_ful[365*(year+1)-1] 
                                                                                      - cumCropValue_ful[365*(year)-1])
                        

