from math import *
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import GPy
from IPython.display import display
from scipy.stats import gamma, gaussian_kde
from . import crop
from . import environment

class farm:
    def __init__(self,name,acreage):
        self.name = name                             # Fun Name for the farm. 
        self.acreage = acreage                       # Size of the farm in acre. 
        self.WaterCost = 542.0                       # Cost of water ($/(acre foot)) 
        self.MinWaterCost = 0.0                      # Water Min Cost is free. 
        self.MaxWaterCost = 542.0                    # Set using DWR Bulletin. 
        self.CashOnHand = 0.0                        # Money Available to purchase Water
        self.cropPortfolioTotalValue = 0.0           # Total Value of all crops on the farm [dollars]. 
        self.cropPortfolioTotalIrrigation = 0.0      # Total Irrigation used on farm [acre feet].
        self.cropPortfolioTotalIrrigationCost = 0.0  # Total Irrigation cost on farm [dollars].

        self.cropPortfolio = []             # The list of crops grown on the farm.
        # (See DWR Bulletin 132-13 Table 14-12 : ($542) per acre foot is the table average
        
    def setSoil(self,fieldCapacity,wiltingPoint):
        self.ThetaFC = fieldCapacity
        self.ThetaWP = wiltingPoint  
        
    # Method with use still to be determined. 
    def setCashOnHand(self,Bank):
        self.CashOnHand = Bank
        
    def setWaterCost(self,Cost):
        self.WaterCost = Cost
    def setMinWaterCost(self,MinCost):
        self.MinWaterCost = MinCost
    def setMaxWaterCost(self,MaxCost):
        self.MaxWaterCost = MaxCost
        
    def setMaxIrrigationPerDay(self,MaxIPerDay):
        # Max Irrigation per day comes in using units of acre feet. 
        ft2mm = 304.8      # mm/ft
        self.MaxIPerDay = MaxIPerDay*ft2mm
        for crop in self.cropPortfolio:
            crop.setMaxIrrigationPerDay(MaxIPerDay*ft2mm)
    
    def PurchaseMMofWaterOnAcres(self,mm,acres):
        # Method deducts from the cash on hand the money required to purchase $mm$ of water 
        # on a specified number of acres of the farm. 
        mmToft = 0.00328084  # mm to feet conversion. 
        TotalCost = mm*mmToft*acres*self.WaterCost
        self.CashOnHand = self.CashOnHand - TotalCost
        return TotalCost
    
    def displayFarm(self):
        mm2ft = 0.00328084 # ft/mm
        print("===================================================================")
        print("Farm: {0}".format(self.name))
        print("     {0} total acres.".format(self.acreage))
        print("     Soil: {0} Water content at field capacity".format(self.ThetaFC))
        print("           {0} Water content at wilting point".format(self.ThetaWP))
        print("     Economy: ${0} \t Cash on hand.".format(self.CashOnHand))
        print("            : ${0} \t Water cost per acre foot.".format(self.WaterCost))
        print("            : {0}  \t Max irrigation per acre per day [feet]".format(mm2ft*self.MaxIPerDay))
        print("===================================================================") 
        
    def addCrop(self,name,area,value,Ky,Ym,Kc,p,Zr,harvestDate,plantDate,simYears):
        NewCrop = crop.crop(name,area,value,Ky,Ym,Kc,p,Zr,harvestDate,plantDate,simYears) 
        NewCrop.TAW = 1000.0*(self.ThetaFC - self.ThetaWP)*NewCrop.Zr
        self.cropPortfolio.append(NewCrop)
        
    def updateAcreage(self,areaVec):
        # areaVec : fractional values of the total acrage available to plant with specified crop. 
        totalArea = self.acreage
        for i in range(len(self.cropPortfolio)-1):
            self.cropPortfolio[i].area = totalArea*areaVec[i] 
            totalArea = totalArea - self.cropPortfolio[i].area
        self.cropPortfolio[-1].area = totalArea
        
    def displayCrops(self):
        for growable in self.cropPortfolio:
            growable.displayCrop()
            
    def displayGrowingSeasons(self):
        for growable in self.cropPortfolio:
            growable.showSeasonDates()
            
            
    # ======================================== #
    # Depreciated function no longer needed.   #
    # As these are being addressed in Yield.   #
    # ======================================== #
    def ComputePortfolioValues(self,environment):
        # Here the crop.CropValue() function call the crop.Yield function that sets
        # values for crop.I and crop.ICost (irrigation water used and irrigation cost) vectors. 
        mm2ft = 0.00328084 # ft/mm

        # Initialize vectors
        self.cropPortfolioValue                 = np.zeros((len(environment.futureData['Precipitation [ft]']),))
        self.cropPortfolioIrrigation            = np.zeros((len(environment.futureData['Precipitation [ft]']),))
        self.cropPortfolioIrrigationCost        = np.zeros((len(environment.futureData['Precipitation [ft]']),))
        self.cropPortfolioIrrigationDeficit     = np.zeros((len(environment.futureData['Precipitation [ft]']),))
        self.cropPortfolioIrrigationDeficitCost = np.zeros((len(environment.futureData['Precipitation [ft]']),))

        self.MaxCropValue = 0
        
        for growable in self.cropPortfolio:    
            # Call crop.CropValue() which also calls yield function. 
            self.cropPortfolioValue             = (self.cropPortfolioValue 
                                                   + growable.CropValue(environment.futureET0,
                                                                        environment.futureData['Precipitation [ft]'],
                                                                        self.ThetaFC,self.ThetaWP,self.WaterCost))
            # Irrigation and Irrigation Deficit are in [feet], Irrigation Cost is [$/acre foot].
            self.cropPortfolioIrrigation            = self.cropPortfolioIrrigation + mm2ft*growable.I*growable.area
            self.cropPortfolioIrrigationCost        = self.cropPortfolioIrrigationCost + growable.ICost
            self.cropPortfolioIrrigationDeficit     = self.cropPortfolioIrrigationDeficit + mm2ft*growable.IDeficit*growable.area
            self.cropPortfolioIrrigationDeficitCost = self.cropPortfolioIrrigationDeficitCost + growable.IDeficitCost
            
            # Compute Potential Max Value = (acres * (annual yield per acre))*(value of yield per acre)*years
            self.MaxCropValue = (self.MaxCropValue 
                                 + growable.area*growable.value*growable.Ym*(len(growable.yearlyGrowing)/365))
            
        # Total cash value of all crops [$]
        self.cropPortfolioTotalValue                 = np.sum(self.cropPortfolioValue)
        # Total of water used for irrigation [ft]. 
        self.cropPortfolioTotalIrrigation            = np.sum(self.cropPortfolioIrrigation)
        # Total cash value of all irrigation used [$].
        self.cropPortfolioTotalIrrigationCost        = np.sum(self.cropPortfolioIrrigationCost)
        # Total Irrigation deficit for all crops [ft].
        self.cropPortfolioTotalIrrigationDeficit     = np.sum(self.cropPortfolioIrrigationDeficit)
        # Total Irrigation deficit Cost for all crops [$].
        self.cropPortfolioTotalIrrigationDeficitCost = np.sum(self.cropPortfolioIrrigationDeficitCost)            
    
            